% Forward-simulation of the policy-state distribution evolutionary
% dynamics with a continuum of agents

%% Generate the static part of the state transition tensor
% Some of the entries of this tensor have a dependency on the social state
% (pi,d), which are updated in the dynamics below
[p_s_down_s_z_a_az_up_sn_zn, p_z_down_s_z_a_az_up_sn_zn] = transition_tensor(param);

% Perceived infection state transition tensor
% This can be different than actual if the fraction of asymptomatic
% agents is unobserved
percv_p_s_down_s_z_a_az_up_sn_zn = p_s_down_s_z_a_az_up_sn_zn;

%% Generate stage reward tensor
r_down_s_z_a_az = reward_tensor(param);

%% DYNAMICS %%
%% Initial social state
pi_down_s_z_up_a_az = param.pi_init_down_s_z_up_a_az;
d_up_s_z = param.d_init_up_s_z;

%% Pleaceholders for previous timestep variables
pi_down_s_z_up_a_az_prev = inf(param.n_s, param.n_z, param.n_a, param.n_z);
d_up_s_z_prev = inf(param.n_s, param.n_z);
V_down_s_z_prev = zeros(param.n_s, param.n_z);

%% Variables requiring initialization
p_s_down_S_z_a_up_S = ones(param.n_z, param.n_a);
percv_p_s_down_S_z_a_up_S = ones(param.n_z, param.n_a);

%% History variables
hist_pi_down_s_z_up_a_az = zeros(param.K, param.n_s, param.n_z, param.n_a, param.n_z);
hist_d_up_s_z = zeros(param.K, param.n_s, param.n_z);
hist_R_down_s_z = zeros(param.K, param.n_s, param.n_z);
hist_V_down_s_z = zeros(param.K, param.n_s, param.n_z);
hist_Q_down_s_z_a_az = zeros(param.K, param.n_s, param.n_z, param.n_a, param.n_z);

hist_mean_a_down_s_z = zeros(param.K, param.n_s, param.n_z);
hist_p_s_down_S_z_up_A = zeros(param.K, param.n_z);

hist_pi_down_s_z_up_a_az(1,:,:,:,:) = pi_down_s_z_up_a_az;
hist_d_up_s_z(1,:,:) = d_up_s_z;

%% Dynamics implementation in loop
k = 1;
pi_diff = abs(pi_down_s_z_up_a_az - pi_down_s_z_up_a_az_prev);
pi_error = max(pi_diff(:));
d_error = norm(d_up_s_z - d_up_s_z_prev, inf);
while true
    %% Display progress
    fprintf('Timestep %d policy error %f distribution error %f\n', k, pi_error, d_error);
    
    %% State tranistions
    % Activation policy
    pi_down_s_z_up_a = sum(pi_down_s_z_up_a_az, 4);
    
    % Mean activation degree
    mean_a_down_s_z = dot2(pi_down_s_z_up_a, param.A, 3, 2);
    
    % Total amount of activity
    e_down_z = sum(d_up_s_z .* mean_a_down_s_z, 1);
    
    % Amount of activity belonging to asymptomatic (s = A) agents
    e_down_z_up_A = d_up_s_z(param.s_A,:) .* mean_a_down_s_z(param.s_A,:);
    
    % Amount of activity belonging to symptomatic (s = I) agents
    e_down_z_up_I = d_up_s_z(param.s_I,:) .* mean_a_down_s_z(param.s_I,:);
    
    % Probability of encountering asymptomatic (s = A) agent
    gamma_down_z_up_A = e_down_z_up_A ./ (e_down_z + param.epsilon);

    % Probability of encountering symptomatic (s = I) agent
    gamma_down_z_up_I = e_down_z_up_I ./ (e_down_z + param.epsilon);

    % Actual probility of infection after activating with degree 1
    p_s_down_S_z_up_A = param.beta_A * gamma_down_z_up_A + param.beta_I * gamma_down_z_up_I;

    % Perceived probility of infection after activating with degree 1
    % Agents could not observe the asymptomatic proportion of agents
    percv_p_s_down_S_z_up_A = param.observe_A * param.beta_A * gamma_down_z_up_A + param.beta_I * gamma_down_z_up_I;
    
    % Probability of no infection after activating with degree 1
    p_s_down_S_z_up_S = 1 - p_s_down_S_z_up_A;
    percv_p_s_down_S_z_up_S = 1 - percv_p_s_down_S_z_up_A;
    
    % Probability of no infection after activating with degree a
    for i_a = 2 : param.n_a     % skip i_a = 1 because this corresponds to no activation
        p_s_down_S_z_a_up_S(:,i_a) = p_s_down_S_z_up_S.^param.A(i_a);
        percv_p_s_down_S_z_a_up_S(:,i_a) = percv_p_s_down_S_z_up_S.^param.A(i_a);
    end
    
    % Probability of infection after activating with degree a
    p_s_down_S_z_a_up_A = 1 - p_s_down_S_z_a_up_S;
    percv_p_s_down_S_z_a_up_A = 1 - percv_p_s_down_S_z_a_up_S;

    % Update dynamic part of state transition tensor
    for i_az = 1 : param.n_z
        for i_zn = 1 : param.n_z
            p_s_down_s_z_a_az_up_sn_zn(param.s_S,:,:,i_az,param.s_S,i_zn) = p_s_down_S_z_a_up_S;
            p_s_down_s_z_a_az_up_sn_zn(param.s_S,:,:,i_az,param.s_A,i_zn) = p_s_down_S_z_a_up_A;
            
            percv_p_s_down_s_z_a_az_up_sn_zn(param.s_S,:,:,i_az,param.s_S,i_zn) = percv_p_s_down_S_z_a_up_S;
            percv_p_s_down_s_z_a_az_up_sn_zn(param.s_S,:,:,i_az,param.s_A,i_zn) = percv_p_s_down_S_z_a_up_A;
        end
    end

    % Combined transition tensor
    p_down_s_z_a_az_up_sn_zn = p_s_down_s_z_a_az_up_sn_zn .* p_z_down_s_z_a_az_up_sn_zn;
    percv_p_down_s_z_a_az_up_sn_zn = percv_p_s_down_s_z_a_az_up_sn_zn .* p_z_down_s_z_a_az_up_sn_zn;

    % Get transition matrix by taking expectation over actions as per policy
    P_down_s_z_up_sn_zn = reshape(dot2(...
        reshape(p_down_s_z_a_az_up_sn_zn, param.n_s, param.n_z, param.n_a * param.n_z, param.n_s, []),...
        reshape(pi_down_s_z_up_a_az, param.n_s, param.n_z, []), 3, 3),...
        param.n_s, param.n_z, param.n_s, []);
    percv_P_down_s_z_up_sn_zn = reshape(dot2(...
        reshape(percv_p_down_s_z_a_az_up_sn_zn, param.n_s, param.n_z, param.n_a * param.n_z, param.n_s, []),...
        reshape(pi_down_s_z_up_a_az, param.n_s, param.n_z, []), 3, 3),...
        param.n_s, param.n_z, param.n_s, []);

    %% Payoffs
    % They use the perceived rather than the actual state transitions
    % Get stage reward vector by taking expectation over actions as per policy
    R_down_s_z = dot2(...
        reshape(r_down_s_z_a_az, param.n_s, param.n_z, []),...
        reshape(pi_down_s_z_up_a_az, param.n_s, param.n_z, []), 3, 3);

    % Get value function
    V_down_s_z = R_down_s_z...
        + param.alpha * dot2(reshape(percv_P_down_s_z_up_sn_zn, param.n_s, param.n_z, []),...
        reshape(V_down_s_z_prev, [], 1), 3, 1);
    V_k = 1;
    while norm(V_down_s_z - V_down_s_z_prev, inf) > param.V_tol && V_k < param.V_K
        V_down_s_z_prev = V_down_s_z;
        V_down_s_z = R_down_s_z...
            + param.alpha * dot2(reshape(percv_P_down_s_z_up_sn_zn, param.n_s, param.n_z, []),...
            reshape(V_down_s_z_prev, [], 1), 3, 1);
        V_k = V_k + 1;
    end

    % Payoff vector
    Q_down_s_z_a_az = r_down_s_z_a_az...
        + param.alpha * dot2(reshape(percv_p_down_s_z_a_az_up_sn_zn, param.n_s, param.n_z, param.n_a, param.n_z, []),...
            reshape(V_down_s_z, [], 1), 5, 1);

    %% Policy update step
    % Perturbed best response
    lambda_Q_down_s_z_a_az = param.lambda * Q_down_s_z_a_az;
    pbr_pi_down_s_z_up_a_az = exp(lambda_Q_down_s_z_a_az - max(lambda_Q_down_s_z_a_az, [], [3, 4])); % Subtract max for numerical stability
    assert(max(pbr_pi_down_s_z_up_a_az(:)) < inf, 'Numerics exploding - decrease lambda');  % Just in case
    pbr_pi_down_s_z_up_a_az = pbr_pi_down_s_z_up_a_az ./ sum(pbr_pi_down_s_z_up_a_az, [3, 4]);
    
    % Asymptomatic and unknowingly recovered agents think they are
    % susceptible
    pbr_pi_down_s_z_up_a_az(param.s_A,:,:,:) = pbr_pi_down_s_z_up_a_az(param.s_S,:,:,:);
    pbr_pi_down_s_z_up_a_az(param.s_U,:,:,:) = pbr_pi_down_s_z_up_a_az(param.s_S,:,:,:);
    
    % Update policy
    pi_down_s_z_up_a_az_prev = pi_down_s_z_up_a_az;
    pi_down_s_z_up_a_az = param.dt * param.eta * pbr_pi_down_s_z_up_a_az + (1 - param.dt * param.eta) * pi_down_s_z_up_a_az;
    pi_down_s_z_up_a_az = pi_down_s_z_up_a_az ./ sum(pi_down_s_z_up_a_az, [3, 4]);   % Corrects for small numerical drifts

    %% State distribution update step
    % Pass through transition matrix
    d_up_s_z_next = reshape(dot2(reshape(d_up_s_z, [], 1),...
        reshape(P_down_s_z_up_sn_zn, [], param.n_s, param.n_z), 1, 1),...
        param.n_s, param.n_z);
    d_up_s_z_next = d_up_s_z_next / sum(d_up_s_z_next(:));  % Corrects for small numerical drifts
    % Update state distribution
    d_up_s_z_prev = d_up_s_z;
    d_up_s_z = param.dt * d_up_s_z_next + (1 - param.dt) * d_up_s_z;
    d_up_s_z = d_up_s_z / sum(d_up_s_z(:));  % Corrects for small numerical drifts
    
    %% Update history
    hist_pi_down_s_z_up_a_az(k+1,:,:,:,:) = pi_down_s_z_up_a_az;
    hist_d_up_s_z(k+1,:,:) = d_up_s_z;
    hist_R_down_s_z(k,:,:) = R_down_s_z;
    hist_V_down_s_z(k,:,:) = V_down_s_z;
    hist_Q_down_s_z_a_az(k,:,:,:,:) = Q_down_s_z_a_az;
    
    hist_mean_a_down_s_z(k,:,:) = mean_a_down_s_z;
    hist_p_s_down_S_z_up_A(k,:) = p_s_down_S_z_up_A;
    
    %% Increment time and update errors
    pi_diff = abs(pi_down_s_z_up_a_az - pi_down_s_z_up_a_az_prev);
    pi_error = max(pi_diff(:));
    d_error = norm(d_up_s_z - d_up_s_z_prev, inf);
    
    if pi_error <= param.se_pi_tol && d_error <= param.se_d_tol || k >= param.K - 1
        break;
    else
        k = k + 1;
    end
    
end

%% Post processing
% Disregard extra history
hist_pi_down_s_z_up_a_az(k+1:end,:,:,:,:) = [];
hist_d_up_s_z(k+1:end,:,:) = [];
hist_R_down_s_z(k+1:end,:,:) = [];
hist_V_down_s_z(k+1:end,:,:) = [];
hist_Q_down_s_z_a_az(k+1:end,:,:,:,:) = [];

hist_mean_a_down_s_z(k+1:end,:,:) = [];
hist_p_s_down_S_z_up_A(k+1:end,:) = [];

% Probability of moving
hist_pi_down_s_z_up_az = sum(hist_pi_down_s_z_up_a_az, 4);
hist_p_m_down_s_z = zeros(k, param.n_s, param.n_z);
for i_z = 1 : param.n_z
    hist_p_m_down_s_z(:,:,i_z) = 1 - hist_pi_down_s_z_up_az(:,:,i_z,i_z);
end

% Amount of movers
hist_move_down_s_z = hist_d_up_s_z .* hist_p_m_down_s_z;
hist_move_down_z = reshape(sum(hist_move_down_s_z, 2), k, []);

% Stage rewards
hist_d_up_s_down_z = hist_d_up_s_z ./ sum(hist_d_up_s_z, 2);
hist_d_up_s_down_z(isnan(hist_d_up_s_down_z)) = 0;
hist_R_down_z = reshape(dot2(hist_d_up_s_down_z, hist_R_down_s_z, 2, 2), k, []);
hist_R = dot2(reshape(hist_d_up_s_z, k, []), reshape(hist_R_down_s_z, k, []), 2);

%% Key performance indicators
% Proportion of agents in the different zones at the end of epidemic
d_up_z = reshape(sum(hist_d_up_s_z(k,:,:), 2), 1, []);

% Proportion of susceptible agents at end of epidemic
d_up_S_down_z = reshape(hist_d_up_s_down_z(k, param.s_S,:), 1, []);
d_up_S = sum(hist_d_up_s_z(k, param.s_S,:));

% Peak of infected state curve
max_d_up_I_z = reshape(max(hist_d_up_s_z(:,param.s_I,:), [], 1), 1, []);
max_d_up_I = max(max_d_up_I_z);

% Total amount of moves
tot_move_down_z = sum(hist_move_down_z, 1) * param.dt;
tot_move = sum(tot_move_down_z);

% Average welfare over length of epidemic
mean_R_down_z = mean(hist_R_down_z, 1);
mean_R = mean(hist_R);

% Length of epidemic, in days
if k >= param.K - 1
    T = inf;    % Dynamics did not convegre, so epidemic did not die out
else
    T = (k - 1) * param.dt;
end

%% Save computation data to file
if ~exist('output', 'dir')
   mkdir('output')
end
if ~exist('output/data', 'dir')
   mkdir('output/data')
end
data_name = 'output/data/data';
switch param.log
    case 1  % alpha is parameter of interest
        data_name = [data_name, '_alpha_', num2str(param.alpha, '%.3f')];
    case 2  % c_m is parameter of interest
        data_name = [data_name, '_c_m_', num2str(param.c_m, '%.3f')];
    case 3  % c_I is parameter of interest
        data_name = [data_name, '_c_I_', num2str(param.c_down_s_z_a(param.s_I,1,end), '%.3f')];
    case 4  % c_S is parameter of interest
        data_name = [data_name, '_c_S_', num2str(param.c_down_s_z_a(param.s_S,1,end), '%.3f')];
    case 5  % delta_U_R is parameter of interest
        data_name = [data_name, '_delta_U_R_', num2str(param.delta_down_U_up_R, '%.3f')];
    case 6  % a_lock is parameter of interest
        data_name = [data_name, '_a_lock_', num2str(param.a_lock(1), '%.3f')];
end
data_name = [data_name, '.mat'];
save(data_name);

%% Make report
report;