function [p_s_down_s_z_a_az_up_sn_zn, p_z_down_s_z_a_az_up_sn_zn] = transition_tensor(param)
% Generates the static part of the state transition tensor, as a function
% of:
% s - Infection state
% z - Zone
% a - Activation degree
% az - Migration zone
% sn - Next infection state
% zn - Next zone
% Note that this tensor has a functional dependency on the social state
% (pi,d), not captured here. It is meant to pre-perform all the static
% transitions, and allow for efficient update of only the dynamic part in
% the real-time dynamics

%% Infection state transitions
p_s_down_s_up_sn = zeros(param.n_s);
p_s_down_s_up_sn(param.s_A,param.s_I) = param.delta_down_A_up_I;
p_s_down_s_up_sn(param.s_A,param.s_U) = param.delta_down_A_up_U;
p_s_down_s_up_sn(param.s_A,param.s_A) = 1 - param.delta_down_A_up_I - param.delta_down_A_up_U;
p_s_down_s_up_sn(param.s_I,param.s_R) = param.delta_down_I_up_R;
p_s_down_s_up_sn(param.s_I,param.s_I) = 1 - param.delta_down_I_up_R;
p_s_down_s_up_sn(param.s_U,param.s_R) = param.delta_down_U_up_R;
p_s_down_s_up_sn(param.s_U,param.s_U) = 1 - param.delta_down_U_up_R;
p_s_down_s_up_sn(param.s_R,param.s_R) = 1;

%% Zone transitions
p_z_down_az_up_zn = eye(param.n_z);

%% Duplicate transition matrices for fast dynamic processing
% Duplicate p_a_down_s_up_sn for state z, actions a, az, and next state
% zn
p_s_down_s_z_a_az_up_sn_zn = zeros(param.n_s, param.n_z, param.n_a, param.n_z, param.n_s, param.n_z);
for i_z = 1 : param.n_z
    for i_a = 1 : param.n_a
        for i_az = 1 : param.n_z
            for i_zn = 1 : param.n_z
                p_s_down_s_z_a_az_up_sn_zn(:,i_z,i_a,i_az,:,i_zn) = p_s_down_s_up_sn;
            end
        end
    end
end

% Duplicate p_z_down_az_up_zn for states s, z, action a, and next state
% sn
p_z_down_s_z_a_az_up_sn_zn = zeros(param.n_s, param.n_z, param.n_a, param.n_z, param.n_s, param.n_z);
for i_s = 1 : param.n_s
    for i_z = 1 : param.n_z
        for i_a = 1 : param.n_a
            for i_sn = 1 : param.n_s
                p_z_down_s_z_a_az_up_sn_zn(i_s,i_z,i_a,:,i_sn,:) = p_z_down_az_up_zn;
            end
        end
    end
end

end