function plot_sweep(sweep_param, sweep_values, sweep_output_z, sweep_output, title, y_lim, param, position)
% Plots the non-infected proportion, as a function of the parameter to sweep
% sweep_param - Index of sweeping parameter (e.g., 1 = alpha)
% sweep_values - Values of sweeping parameter
% sweep_output_z - Output to plot per zone - can be empty to not plot
% sweep_output - Output to plot total over zones - can be empty to not plot
% title - Title of plot
% y_lim - Limits of y-axis for custom scaling
% legend_position - Legend position
% param - Parameters
% position - Used to position and size the plot

%% Plot for each zone, then plot total
n_z = size(sweep_output_z, 2);
lgd_text = cell(1);
for i_z = 1 : n_z
    plot(sweep_values, sweep_output_z(:,i_z), 'LineWidth', 2);
    hold on;
    lgd_text{i_z} = ['Zone ', int2str(param.Z(i_z))];
end
if n_z ~= 1 && ~isempty(sweep_output)
    plot(sweep_values, sweep_output, 'LineWidth', 2);
    lgd_text{n_z+1} = 'Total';
end
axis tight;
if ~isempty(y_lim)
    ylim(y_lim);
end
axes = gca;
axes.FontName = 'Arial';    % Needed for correct html display
axes.Title.String = title;
if n_z > 1
    legend(lgd_text, 'location', 'best');
end
switch sweep_param
    case 1  % alpha is parameter of interest
        xlabel('Future discount factor');
    case 2  % c_m is parameter of interest
        xlabel('Moving cost');
    case 3  % c_I is parameter of interest
        xlabel('Cost of infected activation');
    case 4  % c_S is parameter of interest
        xlabel('Cost of asymptomatic activation');
    case 5  % delta_U_R is parameter of interest
        xlabel('Serological testing rate');
    case 6  % a_lock is parameter of interest
        xlabel('Maximum gatherings');
end
set(gcf, 'Position', position);
end

