function plot_p_S_A(plot_t, hist_p_s_down_S_z_up_A, param, position)
% Plots the probability of infection vs. recovery
% plot_t - Time range of plot
% hist_p_s_down_S_z_up_A - History of infection probability when activating with degree 1 as a function of zone
% param - Parameters
% position - Used to position and size the plot

%% Plot for each zone, then plot probability of recovery
p_s_max = max([max(hist_p_s_down_S_z_up_A(:)), param.delta_down_I_up_R, 0.1]);
plot_k = length(plot_t);
lgd_text = cell(1);
for i_z = 1 : param.n_z
    plot(plot_t, hist_p_s_down_S_z_up_A(:,i_z), 'LineWidth', 2);
    hold on;
    lgd_text{i_z} = ['Zone ', int2str(param.Z(i_z)), ' p(A|S,a=1)'];
end
plot(plot_t, param.delta_down_I_up_R * ones(plot_k,1), 'LineWidth', 2);
lgd_text{end+1} = 'p(R|I)';
axis tight;
ylim([0, p_s_max]);
axes = gca;
axes.FontName = 'Arial';    % Needed for correct html display
axes.Title.String = 'Probability of infection vs. recovery';
legend(lgd_text, 'location', 'best');
xlabel('Time [days]');
set(gcf, 'Position', position);
end

