function plot_p_m(plot_t, hist_p_m_down_s_z, param, position)
% Plots the proportion of movers
% plot_t - Time range of plot
% hist_move_down_s_z - History of proportion of movers as a function of infection state and zone
% param - Parameters
% position - Used to position and size the plot

%% Make the legend
lgd_text = cell(param.n_s, 1);
for i_s = 1 : param.n_s
    lgd_text{i_s} = param.S(i_s);
end

%% Grid over zones
n_rows = min([param.n_z, 2]);   % Maximum 2 rows
n_cols = ceil(param.n_z / n_rows);
move_max = max([max(hist_p_m_down_s_z(:)), 1e-14]);
for i_z = 1 : param.n_z
    subplot(n_rows, n_cols, i_z);
    hold on;
    for i_s = 1 : param.n_s
        plot(plot_t, hist_p_m_down_s_z(:,i_s,i_z), 'LineWidth', 2);
    end
    axis tight;
    ylim([0, move_max]);
    axes = gca;
    axes.FontName = 'Arial';    % Needed for correct html display
    axes.Title.String = ['Zone ', int2str(param.Z(i_z))];
    if i_z == n_cols
        legend(lgd_text, 'location', 'best');   % legend only in top right plot
    end
    if i_z + n_cols > param.n_z
        xlabel('Time [days]');  % xlabel only in bottom row
    end
end
sgtitle('Probability of moving', 'FontName', 'Arial');
set(gcf, 'Position', position);
end

