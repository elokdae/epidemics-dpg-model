function r_down_s_z_a_az = reward_tensor(param)
% Generates the stage reward tensor, as a function of:
% s - Infection state
% z - Zone
% a - Activation degree
% az - Migration zone

%% Stage reward for activation
% Duplicate reward for activation o_down_a for states s and z
r_a_down_s_z_a = zeros(param.n_s, param.n_z, param.n_a);
for i_s = 1 : param.n_s
    r_a_down_s_z_a(i_s,:,:) = repmat(param.o_down_a, param.n_z, 1);
end

% Subtract cost of activation
r_a_down_s_z_a = r_a_down_s_z_a - param.c_down_s_z_a;

%% Stage reward for migration
r_m_down_z_az = -param.c_m * (ones(param.n_z) - eye(param.n_z));

%% Stage reward for illness
r_d_down_s_z_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
r_d_down_s_z_a_az(param.s_I,:,:,:) = -param.c_d;

%% Combine stage rewards for activation and migration
% Duplicate r_a_down_s_z_a for action az
r_a_down_s_z_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
for i_az = 1 : param.n_z
    r_a_down_s_z_a_az(:,:,:,i_az) = r_a_down_s_z_a;
end

% Duplicate r_z_down_z_az for state s and action a
r_z_down_s_z_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
for i_s = 1 : param.n_s
    for i_a = 1 : param.n_a
        r_z_down_s_z_a_az(i_s,:,i_a,:) = r_m_down_z_az;
    end
end

% Combine the stage rewards
r_down_s_z_a_az = r_a_down_s_z_a_az + r_z_down_s_z_a_az + r_d_down_s_z_a_az;

end

