% Runs the model once
clc;
close all;
clear;
addpath('functions');

%% Load parameters
param = load_parameters();

%% Run dynamics
model;