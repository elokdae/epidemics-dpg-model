% Creates a report of the results
close all;
addpath('functions');

%% Initialize folders to save report and figures
if ~exist('output', 'dir')
   mkdir('output')
end
if ~exist('output/reports', 'dir')
   mkdir('output/reports')
end
if ~exist('output/figures', 'dir')
   mkdir('output/figures')
end

%% Initialize report
import mlreportgen.report.*
import mlreportgen.dom.*
rpt_name = 'output/reports/report';
switch param.log
    case 1  % alpha is parameter of interest
        log_value = param.alpha;
        log_string = 'alpha';
    case 2  % c_m is parameter of interest
        log_value = param.c_m;
        log_string = 'c_m';
    case 3  % c_I is parameter of interest
        log_value = param.c_down_s_z_a(param.s_I,1,end);
        log_string = 'c_I';
    case 4  % c_S is parameter of interest
        log_value = param.c_down_s_z_a(param.s_S,1,end);
        log_string = 'c_S';
    case 5  % delta_U_R is parameter of interest
        log_value = param.delta_down_U_up_R;
        log_string = 'delta_U_R';
    case 6  % a_lock is parameter of interest
        log_value = param.a_lock(1);
        log_string = 'a_lock';
end
if param.log ~= 0
    rpt_name = [rpt_name, '_', log_string, '_', num2str(log_value, '%.3f')];
end
rpt = Report(rpt_name, 'html-file');
rpt.TemplatePath = 'templates/report.htmt';

%% Title
tp = TitlePage;
title = 'Epidemic Report';
if param.log ~= 0
    title = [title, ' for ', log_string, ' = ', num2str(log_value, '%.3f')];
end
tp.Title = title;
tp.Author = [];
tp.PubDate = [];
tp.TemplateSrc = 'templates/titlepage.htmt';
add(rpt, tp);

%% Key performance indicators
tbl_cell = cell(1, param.n_z + 2);
tbl_cell{1,1} = '';
tbl_cell{2,1} = 'Proportion of population';
tbl_cell{3,1} = 'Non-infected proportion';
tbl_cell{4,1} = 'Peak infections';
tbl_cell{5,1} = 'Total moves';
tbl_cell{6,1} = 'Average welfare';
tbl_cell{7,1} = 'Epidemic duration [days]';

for i_z = 1 : param.n_z
    tbl_cell{1,i_z+1} = ['Zone ', int2str(param.Z(i_z))];
    tbl_cell{2,i_z+1} = num2str(d_up_z(i_z), '%.3f');
    tbl_cell{3,i_z+1} = num2str(d_up_S_down_z(i_z), '%.3f');
    tbl_cell{4,i_z+1} = num2str(max_d_up_I_z(i_z), '%.3f');
    tbl_cell{5,i_z+1} = num2str(tot_move_down_z(i_z), '%.3f');
    tbl_cell{6,i_z+1} = num2str(mean_R_down_z(i_z), '%.3f');
end
tbl_cell{1,end} = 'Total';
tbl_cell{2,end} = num2str(sum(d_up_z), '%.3f');
tbl_cell{3,end} = num2str(d_up_S, '%.3f');
tbl_cell{4,end} = num2str(max_d_up_I, '%.3f');
tbl_cell{5,end} = num2str(tot_move, '%.3f');
tbl_cell{6,end} = num2str(mean_R, '%.3f');
tbl_cell{7,end} = num2str(T, '%.0f');

tbl = Table(tbl_cell);
tbl.Style = {RowSep('solid','black','1px'),...
    OuterMargin('20pt', '0pt', '0pt', '0pt')}; 
tbl.TableEntriesStyle = {FontFamily('Arial'),...
    InnerMargin('10pt', '10pt')};

sec = Section;
sec.Title = sprintf('\tKPIs');
sec.Numbered = false;
sec.TemplateSrc = 'templates/section.htmt';
add(sec, tbl);
add(rpt, sec);

%% Screen size used to size plots
screen_size = get(groot, 'ScreenSize');
screen_width = screen_size(3);
screen_height = screen_size(4);
default_width = screen_width / 3.1;
default_height = screen_height / 3.1;
default_position = [0, default_height, default_width, default_height];

%% Time range to plot
plot_T = min([T, 500]);
plot_k = plot_T / param.dt + 1;
plot_t = 0 : param.dt : plot_T;

%% State distribution
fig = figure('visible', 'off');
plot_d(plot_t, hist_d_up_s_z(1:plot_k,:,:), param, default_position);
print(fig, '-dsvg', 'output/figures/d_plot.svg');
d_img = Image('output/figures/d_plot.svg');
delete(gcf);

%% Mean activation degree
fig = figure('visible', 'off');
plot_mean_a(plot_t, hist_mean_a_down_s_z(1:plot_k,:,:), param, default_position);
print(fig, '-dsvg', 'output/figures/mean_a_plot.svg');
mean_a_img = Image('output/figures/mean_a_plot.svg');
close(gcf);

%% Probability of moving
fig = figure('visible', 'off');
plot_p_m(plot_t, hist_p_m_down_s_z(1:plot_k,:,:), param, default_position);
print(fig, '-dsvg', 'output/figures/p_m_plot.svg');
move_img = Image('output/figures/p_m_plot.svg');
close(gcf);

%% Value function
fig = figure('visible', 'off');
plot_V(plot_t, hist_V_down_s_z(1:plot_k,:,:), param, default_position);
print(fig, '-dsvg', 'output/figures/V_plot.svg');
V_img = Image('output/figures/V_plot.svg');
close(gcf);

%% Infection state transition probabilities
fig = figure('visible', 'off');
plot_p_S_A(plot_t, hist_p_s_down_S_z_up_A(1:plot_k,:), param, default_position);
print(fig, '-dsvg', 'output/figures/p_S_A_plot.svg');
p_S_A_img = Image('output/figures/p_S_A_plot.svg');
close(gcf);

%% Finish up and display report
sec = Section;
sec.Title = sprintf('\tTrends');
sec.Numbered = false;
sec.TemplateSrc = 'templates/section.htmt';
tbl = Table({d_img, mean_a_img, move_img; V_img, p_S_A_img, []});
add(sec, tbl);
add(rpt, sec);
close(rpt);
if ~exist('sweep_param', 'var')
    rptview(rpt);   % Show report only if not doing a parameter sweep (it is slow)
end