% Creates a report of the results from a parameter sweep
close all;
addpath('functions');

%% Parameter to sweep
% Same convention as param.log
% 0 (default) = no parameter
% 1 = alpha
% 2 = c_m
% 3 = c_I
% 4 = c_S
% 5 = delta_U_R
% 6 = a_lockdown
if ~exist('sweep_param', 'var')
    sweep_param = 5;

    switch sweep_param
        case 1  % alpha is parameter of interest
            param_string = 'alpha';
            sweep_values = 0.50 : 0.025 : 0.975;
        case 2  % c_m is parameter of interest
            param_string = 'c_m';
            sweep_values = 0 : 10;
        case 3  % c_I is parameter of interest
            param_string = 'c_I';
            sweep_values = [0 : 0.25 : 0.75, 0.80 : 0.05 : 1.25, 1.5 : 0.25 : 3.00];
        case 4  % c_S is parameter of interest
            param_string = 'c_S';
            sweep_values = [0 : 0.1 : 0.5, 0.55 : 0.05 : 1.0];
        case 5 % delta_U_R is parameter of interest
            param_string = 'delta_U_R';
            sweep_values = 0.0 : 0.05 : 1.0;
        case 6 % a_lock is parameter of interest
            param_string = 'a_lock';
            sweep_values = 6 : -1 : 0;
    end
    
    sweep_length = length(sweep_values);
end

switch sweep_param
    case 1  % alpha is parameter of interest
        plot_values = [0.5, 0.65, 0.75, 0.9, 0.975];
    case 2  % c_M is parameter of interest
        plot_values = [0, 1, 2, 5, 8, 10];
    case 3  % c_I is parameter of interest
        plot_values = [0, 0.8, 1, 1.5, 2, 3];
    case 4  % c_S is parameter of interest
        plot_values = [0, 0.5, 0.75, 0.85, 1];
    case 5  % delta_U_R is parameter of interest
        plot_values = [0.0, 0.1, 0.5, 1.0];
    case 6  % a_lock is parameter of interest
        plot_values = sweep_values;
end

plot_length = length(plot_values);

%% Initialize folders to save report and figures
if ~exist('output', 'dir')
   mkdir('output')
end
if ~exist('output/reports', 'dir')
   mkdir('output/reports')
end
if ~exist('output/figures', 'dir')
   mkdir('output/figures')
end

%% Initialize report
import mlreportgen.report.*
import mlreportgen.dom.*
rpt_name = 'output/reports/report';
rpt_name = [rpt_name, '_', param_string, '_sweep'];
rpt = Report(rpt_name, 'html-file');
rpt.TemplatePath = 'templates/report.htmt';

%% Title
tp = TitlePage;
title = 'Epidemic Report';
title = [title, ' for ', param_string, ' Sweep'];
tp.Title = title;
tp.Author = [];
tp.PubDate = [];
tp.TemplateSrc = 'templates/titlepage.htmt';
add(rpt, tp);

%% Screen size used to size plots
screen_size = get(groot, 'ScreenSize');
screen_width = screen_size(3);
screen_height = screen_size(4);
default_width = screen_width / 5.1;
default_height = screen_height / 5.1;
default_position_KPI = [0, default_height, default_width, default_height];
default_position_trend = [0, default_height, screen_width / 5.3, screen_height / 5.3 * 2];

%% Key performance indicators
% Collect from data files
data_name_hdr = 'output/data/data';
% data_name_hdr = 'output/ser_test/a_lock_1_alpha_0.00/data/data';
data_name_hdr = [data_name_hdr, '_', param_string, '_'];
for i_sweep = 1 : sweep_length
    data_name = [data_name_hdr, num2str(sweep_values(i_sweep), '%.3f'), '.mat'];
    data = load(data_name);
    
    if i_sweep == 1 % Initialize placeholders
        sweep_d_up_S_down_z = zeros(sweep_length, data.param.n_z);
        sweep_max_d_up_I_z = zeros(sweep_length, data.param.n_z);
        sweep_tot_move_down_z = zeros(sweep_length, data.param.n_z);
        sweep_mean_R_down_z = zeros(sweep_length, data.param.n_z);
        
        sweep_d_up_S = zeros(sweep_length, 1);
        sweep_max_d_up_I = zeros(sweep_length, 1);
        sweep_tot_move = zeros(sweep_length, 1);
        sweep_mean_R = zeros(sweep_length, 1);
        sweep_T = zeros(sweep_length, 1);
    end
    
    sweep_d_up_S_down_z(i_sweep,:) = data.d_up_S_down_z;
    sweep_max_d_up_I_z(i_sweep,:) = data.max_d_up_I_z;
    sweep_tot_move_down_z(i_sweep,:) = data.tot_move_down_z;
    sweep_mean_R_down_z(i_sweep,:) = data.mean_R_down_z;

    sweep_d_up_S(i_sweep) = data.d_up_S;
    sweep_max_d_up_I(i_sweep) = data.max_d_up_I;
    sweep_tot_move(i_sweep) = data.tot_move;
    sweep_mean_R(i_sweep) = data.mean_R;
    sweep_T(i_sweep) = data.T;
    
    switch sweep_param
        case 3  % c_I is parameter of interest
            % Do not count the cost of infected activations in the average
            % welfare
            hist_pi_down_I_z_up_a = reshape(sum(data.hist_pi_down_s_z_up_a_az(:,data.param.s_I,:,:,:), 5), data.k, data.param.n_z, []);
            hist_pi_C_I_down_z = dot2(hist_pi_down_I_z_up_a, data.param.C_I_down_z_a, 3, 2);
            hist_C_I_down_z = hist_pi_C_I_down_z .* reshape(data.hist_d_up_s_down_z(:,data.param.s_I,:), data.k, []);
            hist_C_I = dot2(reshape(data.hist_d_up_s_z(:,data.param.s_I,:), data.k, []), hist_pi_C_I_down_z, 2, 2);
            
            sweep_mean_R_down_z(i_sweep,:) = sweep_mean_R_down_z(i_sweep,:) + mean(hist_C_I_down_z, 1);
            sweep_mean_R(i_sweep) = sweep_mean_R(i_sweep) + mean(hist_C_I);
            
        case 4  % c_S is parameter of interest
            % Do not count the cost of susceptible and asymptomatic activations in the average
            % welfare
            hist_pi_down_A_z_up_a = reshape(sum(data.hist_pi_down_s_z_up_a_az(:,data.param.s_A,:,:,:), 5), data.k, data.param.n_z, []);
            hist_pi_C_A_down_z = dot2(hist_pi_down_A_z_up_a, data.param.C_A_down_z_a, 3, 2);
            hist_C_A_down_z = hist_pi_C_A_down_z .* reshape(data.hist_d_up_s_down_z(:,data.param.s_S,:) + data.hist_d_up_s_down_z(:,data.param.s_A,:), data.k, []);
            hist_C_A = dot2(reshape(data.hist_d_up_s_z(:,data.param.s_S,:) + data.hist_d_up_s_z(:,data.param.s_A,:), data.k, []), hist_pi_C_A_down_z, 2, 2);
            
            sweep_mean_R_down_z(i_sweep,:) = sweep_mean_R_down_z(i_sweep,:) + mean(hist_C_A_down_z, 1);
            sweep_mean_R(i_sweep) = sweep_mean_R(i_sweep) + mean(hist_C_A);
    end
end


%% Non-infected proportion
fig = figure('visible', 'off');
switch sweep_param
    case 2  % c_m is parameter of interest
        y_lim = [0, max([max(sweep_d_up_S_down_z(1:end-1,:), [], 'all'), max(sweep_d_up_S)])];
        plot_sweep(sweep_param, sweep_values, sweep_d_up_S_down_z, sweep_d_up_S, 'Non-infected proportion', y_lim, data.param, default_position_KPI) 
    otherwise
        plot_sweep(sweep_param, sweep_values, [], sweep_d_up_S, 'Non-infected proportion', [], data.param, default_position_KPI);
end
print(fig, '-dsvg', 'output/figures/d_up_S_plot.svg');
d_up_S_img = Image('output/figures/d_up_S_plot.svg');
delete(gcf);

%% Peak infections
fig = figure('visible', 'off');
plot_sweep(sweep_param, sweep_values, sweep_max_d_up_I_z, [], 'Peak infections', [], data.param, default_position_KPI);
print(fig, '-dsvg', 'output/figures/max_d_up_I_plot.svg');
max_d_up_I_img = Image('output/figures/max_d_up_I_plot.svg');
delete(gcf);

%% Total moves
fig = figure('visible', 'off');
switch sweep_param
    case 2  % c_m is parameter of interest
        y_lim = [0, max(sweep_tot_move_down_z(2:end,:), [], 'all')];
        plot_sweep(sweep_param, sweep_values, sweep_tot_move_down_z, [], 'Total moves', y_lim, data.param, default_position_KPI);
    otherwise
        plot_sweep(sweep_param, sweep_values, sweep_tot_move_down_z, [], 'Total moves', [], data.param, default_position_KPI);
end
print(fig, '-dsvg', 'output/figures/tot_move_plot.svg');
tot_move_img = Image('output/figures/tot_move_plot.svg');
delete(gcf);

%% Average welfare
fig = figure('visible', 'off');
switch sweep_param
    case 2  % c_m is parameter of interest
        y_lim = [min([0, min(sweep_mean_R_down_z(:)), min(sweep_mean_R)]), max([max(sweep_mean_R_down_z(1:end-1,:), [], 'all'), max(sweep_mean_R)])];
        plot_sweep(sweep_param, sweep_values, sweep_mean_R_down_z, sweep_mean_R, 'Average welfare', y_lim, data.param, default_position_KPI);
    case 3  % c_I is parameter of interest
        plot_sweep(sweep_param, sweep_values, [], sweep_mean_R, 'Average welfare (without infected activation cost)', [], data.param, default_position_KPI);
    case 4  % c_S is parameter of interest
        plot_sweep(sweep_param, sweep_values, [], sweep_mean_R, 'Average welfare (without asymptomatic activation cost)', [], data.param, default_position_KPI);
    otherwise
        plot_sweep(sweep_param, sweep_values, [], sweep_mean_R, 'Average welfare', [], data.param, default_position_KPI);
end
print(fig, '-dsvg', 'output/figures/mean_R_plot.svg');
mean_R_img = Image('output/figures/mean_R_plot.svg');
delete(gcf);

%% Epidemic duration
fig = figure('visible', 'off');
plot_sweep(sweep_param, sweep_values, [], sweep_T, 'Epidemic duration [days]', [], data.param, default_position_KPI);
print(fig, '-dsvg', 'output/figures/T_plot.svg');
T_img = Image('output/figures/T_plot.svg');
delete(gcf);

%% Add KPIs section
sec = Section;
sec.Title = sprintf('\tKPIs');
sec.Numbered = false;
sec.TemplateSrc = 'templates/section.htmt';
tbl = Table({d_up_S_img, max_d_up_I_img, tot_move_img, mean_R_img, T_img});
add(sec, tbl);
add(rpt, sec);

%% Plot trends for selected parameter values
tbl_cell = cell(1);
for i_plot = 1 : plot_length
    data_name = [data_name_hdr, num2str(plot_values(i_plot), '%.3f'), '.mat'];
    data = load(data_name);
    tbl_cell{i_plot,1} = [param_string, ' = ', num2str(plot_values(i_plot), '%.3f')];
    
    %% Time range to plot
    plot_T = 150;
    plot_k = plot_T / data.param.dt + 1;
    plot_t = 0 : data.param.dt : plot_T;

    %% State distribution
    fig = figure('visible', 'off');
    plot_d(plot_t, data.hist_d_up_s_z(1:plot_k,:,:), data.param, default_position_trend);
    print(fig, '-dsvg', ['output/figures/d_plot_', int2str(i_plot), '.svg']);
    tbl_cell{i_plot,2} = Image(['output/figures/d_plot_', int2str(i_plot), '.svg']);
    delete(gcf);

    %% Mean activation degree
    fig = figure('visible', 'off');
    plot_mean_a(plot_t, data.hist_mean_a_down_s_z(1:plot_k,:,:), data.param, default_position_trend);
    print(fig, '-dsvg', ['output/figures/mean_a_plot_', int2str(i_plot), '.svg']);
    tbl_cell{i_plot,3} = Image(['output/figures/mean_a_plot_', int2str(i_plot), '.svg']);
    close(gcf);

    %% Probability of moving
    fig = figure('visible', 'off');
    plot_p_m(plot_t, data.hist_p_m_down_s_z(1:plot_k,:,:), data.param, default_position_trend);
    print(fig, '-dsvg', ['output/figures/p_m_plot_', int2str(i_plot), '.svg']);
    tbl_cell{i_plot,4} = Image(['output/figures/p_m_plot_', int2str(i_plot), '.svg']);
    close(gcf);

    %% Value function
    fig = figure('visible', 'off');
    plot_V(plot_t, data.hist_V_down_s_z(1:plot_k,:,:), data.param, default_position_trend);
    print(fig, '-dsvg', ['output/figures/V_plot_', int2str(i_plot), '.svg']);
    tbl_cell{i_plot,5} = Image(['output/figures/V_plot_', int2str(i_plot), '.svg']);
    close(gcf);

    %% Infection state transition probabilities
    fig = figure('visible', 'off');
    plot_p_S_A(plot_t, data.hist_p_s_down_S_z_up_A(1:plot_k,:), data.param, default_position_trend);
    print(fig, '-dsvg', ['output/figures/p_S_A_plot_', int2str(i_plot), '.svg']);
    tbl_cell{i_plot,6} = Image(['output/figures/p_S_A_plot_', int2str(i_plot), '.svg']);
    close(gcf);
end

%% Finish up and display report
sec = Section;
sec.Title = sprintf('\tTrends');
sec.Numbered = false;
sec.TemplateSrc = 'templates/section.htmt';
tbl = Table(tbl_cell);
tbl.TableEntriesStyle = {Bold};
add(sec, tbl);
add(rpt, sec);
close(rpt);
rptview(rpt);