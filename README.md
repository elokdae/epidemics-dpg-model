# epidemics-dpg-model

Software and paper source code for "A dynamic population model of strategic interaction and migration under epidemic risk" (https://doi.org/10.1109/CDC45484.2021.9683739).

Authors: Ezzat Elokda, Saverio Bolognani, Ashish R. Hota

# Software Dependencies

Multiprod v1.37.0.0 - https://www.mathworks.com/matlabcentral/fileexchange/8773-multiple-matrix-multiplications-with-array-expansion-enabled

Vector algebra v1.3.0.0 - https://www.mathworks.com/matlabcentral/fileexchange/8782-vector-algebra-for-arrays-of-any-size-with-array-expansion-enabled

# Instructions to run software

1. Select parameters in load_parameters.m

2. Run main.m

3. A report with the results will appear

# Instructions to compile paper from Latex source
1. Compile paper-latex/main.tex
    - It is recommended to use TexStudio. Accept the prompt to override the compile command with "(a) allow for this document"
    - If not using TexStudio, configure editor to use following compile command: `pdflatex -synctex=1 -interaction=nonstopmode -shell-escape %.tex`
2. Individual pdf figures will be generated in tikz/ folder
3. Compiled paper is main.pdf

# License information
Refer to LICENSE.txt for software and paper-latex/LICENSE.txt for paper source code
