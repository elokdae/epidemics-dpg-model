% Runs the model multiple times to perform specified parameter sweep
clc;
close all;
clear;
addpath('functions');

%% Load parameters
% This loads the default parameters, and we will be changing the ones to
% sweep later
param = load_parameters();

%% Parameter to sweep
% Same convention as param.log
% 0 (default) = no parameter
% 1 = alpha
% 2 = c_m
% 3 = c_I
% 4 = c_S
% 5 = delta_U_R
% 6 = a_lock
sweep_param = 6;
param.log = sweep_param;

switch sweep_param
    case 1  % alpha is parameter of interest
        param_string = 'alpha';
        sweep_values = 0.50 : 0.025 : 0.975;
    case 2  % c_m is parameter of interest
        param_string = 'c_m';
        sweep_values = 0 : 10;
    case 3  % c_I is parameter of interest
        param_string = 'c_I';
        sweep_values = [0 : 0.25 : 0.75, 0.80 : 0.05 : 1.25, 1.5 : 0.25 : 3.00];
    case 4  % c_S is parameter of interest
        param_string = 'c_S';
        sweep_values = [0 : 0.1 : 0.5, 0.55 : 0.05 : 1.0];
    case 5 % delta_U_R is parameter of interest
        param_string = 'delta_U_R';
        sweep_values = 0.0 : 0.05 : 1.0;
    case 6 % a_lock is parameter of interest
        param_string = 'a_lock';
        sweep_values = param.a_max : -1 : 0;
end

sweep_length = length(sweep_values);

%% Perform sweep
for i_sweep = 1 : sweep_length
    param_value = sweep_values(i_sweep);
    
    %% Display progress
    fprintf('\n\nSweep # %d parameter %s value %.3f\n\n', i_sweep , param_string, param_value);
    pause(3);
    
    %% Update parameter value
    switch sweep_param
        case 1  % alpha is parameter of interest
            param.alpha = param_value;
        case 2  % c_m is parameter of interest
            param.c_m = param_value;
        case 3  % c_I is parameter of interest
            param.c_down_s_z_a(param.s_I,1,end) = param.value;
        case 4  % c_S is parameter of interest
            param.c_down_s_z_a(param.s_S,1,end) = param.value;
        case 5 % delta_U_R is parameter of interest
            param.delta_down_U_up_R = param_value;
        case 6 % a_lock is parameter of interest
            param.a_lock(:) = param_value;
            i_a_lock = param.a_lock + 1;
            for i_z = 1 : param.n_z
                param.c_down_s_z_a(param.s_S,i_z,1:i_a_lock(i_z)) = 0.0 * param.o_down_a(1:i_a_lock(i_z));
                param.c_down_s_z_a(param.s_S,i_z,i_a_lock(i_z)+1:end) = 3.0 * param.o_down_a(i_a_lock(i_z)+1:end);
            end
            param.c_down_s_z_a(param.s_A,:,:) = param.c_down_s_z_a(param.s_S,:,:);
            param.c_down_s_z_a(param.s_U,:,:) = param.c_down_s_z_a(param.s_S,:,:);
            if param.R_lock     % Shutdown R as well?
                param.c_down_s_z_a(param.s_R,:,:) = param.c_down_s_z_a(param.s_S,:,:);
            end
    end
    
    %% Run model
    % It will be storing the data and generating the reports
    model;
end

%% Generate performance comparison report
performance_comparison;