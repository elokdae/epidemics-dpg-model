\section{Model}
\label{sec:model}

We consider a homogeneous population of \ezz{non-atomic} agents or individuals. The state and the dynamics of this population are described by the following elements.

% \begin{itemize}
%     \item Zone $z \in \Z = \set{1,\dots,\zmax}$,
%     \item Bounded rationality $\lambda \in \Lambda = \set{\lambda_1,\dots,\lambdamax}$,
%     \item Future discount factor $\alpha \in A = \set{\alpha_1,\dots,\alphamax}$.
% \end{itemize}
% The concatenation is $\tau = (z,\lambda,\alpha) \in T = \Z \times \Lambda \times A$.

% The type distribution, a parameter, is $g \in \Delta^{|T|}$, with $g_\tau = g_{z,\lambda,\alpha}$ the proportion of agents of the different types. With a slight abuse of notation, we can write the marginals $g_z = \sum_{\lambda,\alpha} g_{z,\lambda,\alpha}$, $g_\lambda = \sum_{z,\alpha} g_{z,\lambda,\alpha}$, $g_\alpha = \sum_{z,\lambda} g_{z,\lambda,\alpha}$ to denote the proportion of agents of the different zones, bounded rationality, and future awareness. If these are independent, then $g_{z,\lambda,\alpha} = g_z \: g_\lambda \: g_\alpha$.

%(and hence immune from further infection)

\subsection{States}

We augment the SAIR epidemic model to distinguish between recovered agents who are aware of being recovered and those who are recovered, but unaware of ever being infected. Specifically, each agent can be in one of the following infection states: \emph{Susceptible} ($\Sus$), \emph{infected Asymptomatic} ($\Asym$), \emph{Infected symptomatic} ($\Inf$), \emph{Recovered} ($\Rec$), \emph{Unknowingly recovered} ($\Unk$) (agents that have recovered without showing symptoms). Agents in states $\Rec$ and $\Unk$ are immune from further infection. Moreover, each agent resides in one of $Z$ \emph{zones} or locations. Formally, we define the state of each agent as $(s,z) \in \St \times \Z$, where $\St = \set{\Sus,\Asym,\Inf,\Rec,\Unk}$ and $\Z = \set{1,\dots,Z}$. The state distribution is $d \in \D = \Delta(\St \times \Z)$, where \ezz{$\Delta(X)$ is the space of probability distributions supported in $X$. We write} $d[s,z]$ \ezz{to denote} the proportion of agents with infection state $s$ residing in zone $z$. 

\subsection{Actions and policies}

We consider a dynamic environment that evolves in discrete-time (e.g., each time interval representing a day).  
%This discretization is aligned with the natural discretization at which individuals take decisions about their activity in society, and is sufficiently fine to describe the illness progression.
At each time step, each agent strategically chooses:
\begin{itemize}
    \item its activation degree $a \in \A = \set{0,1,\dots,\amax}$, which denotes the number of other agents it chooses to interact with ($a=0$ signifies no activation), and
    \item the zone $\mz \in \Z$ where to move for the next day.
\end{itemize}
The combined action is denoted $(a,\mz) \in \A \times \Z$. A (Markovian) policy is denoted by $\pi : \St \times \Z \rightarrow \Delta(A \times \Z)$, and it maps an agent's state $(s,z) \in \St \times \Z$ to a randomization over the actions $(a,\mz) \in \A \times \Z$. 
The set of all possible policies is denoted by $\Pi$.
Explicitly, $\pi[a,\mz \mid s,z]$ is the probability that an agent plays $(a,\mz)$ when in state $(s,z)$. All agents are homogeneous and follow the same policy $\pi$. Further, agents that have never shown symptoms act in the same way, i.e.  $\pi[\cdot \mid \Sus,z] = \pi[\cdot \mid \Asym,z] = \pi[\cdot \mid \Unk,z]$. Note that $\pi$ is \emph{time-varying}; agents change their strategies as the epidemic unfolds. The dynamics of $\pi$ are detailed in Section~\ref{sec:socdynamics}.

%Policies need to be consistent with the information structure of the problem: 
%and constitute an important part of our model since they affect the transient evolution of the epidemic.}

%We subsequently discuss how the policy evolves in time (denoting the learning process of the agents) and characterize stationary policies that arise at an equilibrium. 

% Because of the homogeneity of the agents, we consider only \emph{symmetric policies}, i.e., we assume that all the agents follow the same policy $\pi$.

% \subsection{Social state}

% The concatenation of the policy and state distribution is the \emph{social state} $(\pi,d) \in \Pi \times \D$. This gives a complete macroscopic description of the distribution of the agents' states, as well as how they behave. The social state $(\pi,d)$ is a time-varying quantity which evolves in continuous time, as described later. 

\subsection{State transitions}
\label{ssec:statetransitions}

%In order to study the dynamics of the  when agents adopt a policy $\pi$, we now derive a model for the state dynamics of an individual agent that is following such policy.

We now derive a dynamic model of the evolution of state distribution $d$ when the agents adopt a policy $\pi$. We denote the policy-state pair $(\pi,d)$ as the {\it social state}. The state of each agent changes at every time step according to transition probabilities encoded by the \emph{stochastic matrix}
\begin{multline}
    P[s^+,z^+ \mid s,z](\pi,d) \\= \sum_{a,\mz} \pi[a,\mz \mid s,z] \: p[s^+,z^+ \mid s,z,a,\mz](\pi,d),
    \label{eq:stochasticMatrix}
\end{multline}
where $p[s^+,z^+ \mid s,z,a,\mz](\pi,d)$ denotes the probability distribution over the next state when an agent in infection state $s$ and zone $z$ chooses action $(a,\tilde{z}$) in social state $(\pi,d)$.

%However, it becomes time-homogeneous if $(\pi,d)$ is stationary (see the equilibrium analysis in Section~\ref{sec:equilibrium}).

Note that the Markov chain $P[s^+,z^+ \mid s,z](\pi,d)$ is not time-homogeneous as the social state $(\pi,d)$ is time-varying. The state transition function is defined as
\begin{multline}
    p[s^+,z^+ \mid s,z,a,\mz](\pi,d) \\
    := \prob[s^+ \mid s,z,a](\pi,d) \: \prob[z^+ \mid z, \mz](\pi,d). \label{eq:Transitions}
\end{multline}

The zone transition probabilities are assumed to be independent of $(\pi,d)$ and given by
\[
    \prob[z^+ \mid z, \mz](\pi,d) = \begin{cases}
        1 & \text{if } z^+ = \mz, \\
        0 & \text{otherwise}.
    \end{cases}
\]

In order to derive the infection state transition probabilities $\prob[s^+ \mid s,z,a](\pi,d)$ (which are schematically represented in Figure~\ref{fig:SAIR_transitions}), we combine the transition rules of the (augmented) SAIR model with the specific activation actions as follows. 
\begin{itemize}
\item At a given time, an agent in state $s$ in zone $z$ chooses its activation degree $a$ according to policy $\pi$. Then, it is paired randomly with \ezz{up to} $a$ other individuals in zone $z$ with the probability of being connected with another agent being proportional to the activation degree of the target agent (analogous to the configuration model \cite{newman2010networks}).
\item \ezz{The agent could also fail to pair with one or more of the $a$ other individuals. This occurs with increasing probability as the \emph{total amount of activity} in the zone (to be defined hereafter) is low\footnote{\ezz{This represents, for example, when the public space (streets, buildings) are largely empty because most agents are staying at home.}}.} 
\item Once the network is formed, a susceptible agent becomes asymptomatic\ezz{ally infected} with probability $\beta_\Asym \in [0,1]$ per each asymptomatic neighbor and with probability $\beta_\Inf \in [0,1]$ per each infected neighbor. 
%\ezz{The infection always starts in the asymptomatic state as there is a typical delay in the onset of symptoms.}
\item An asymptomatic agent becomes (symptomatically) infected\footnote{This represents both the aggravation of the illness and the outcome of testing in the asymptomatic population.} with probability $\delta_\Asym^\Inf \in (0,1]$, and recovers without being aware of it with probability $\delta_\Asym^\Unk \in [0,1]$. 

%The probability of transitioning from asymptomatic to infected is , and represent both the %\in (0,1]$, 
%\item The probability of transitioning from asymptomatic to unknowingly recovered is . %\in [0,1-\delta_\Asym^\Inf]$.
    %, and to remain asymptomatic is $1 - \delta_\Asym^\Inf - \delta_\Asym^\Rec$.
    
\item An infected agent recovers with probability $\delta_\Inf^\Rec \in (0,1]$.
\item An individual in state $\Unk$ becomes aware of its recovery with probability $\delta_\Unk^\Rec \in [0,1]$ (for example via serological tests on the population). 
%Agents in state $\Unk$ and $\Rec$ do not get infected again.
\item The network thus formed gets discarded at the next time step and the process repeats.
\end{itemize}

%Moreover, we denote by  the probability that an agent that has unknowingly recovered realizes that they have in fact recovered and that they are immune .%Let us then compute the probability of interacting with asymptomatic or infected agents \ezz{per degree of activation}. 

%We now formally state the infection state transition probabilities. 

Note that with the exception of the transition from state $\Sus$ to $\Asym$, all other state transition probabilities are defined via exogenous parameters and do not depend on the social state. In order to compute the transition probability from $\Sus$ to $\Asym$, we define the \emph{total amount \ezz{or mass} of activity} in zone $z$ as
\begin{equation*}
    e_z(\pi,d) = \sum_s d[s,z] \: \sum_{a,\mz} a \: \pi[a,\mz \mid s,z],
\end{equation*}
which is determined by the mass of active agents and their degrees of activation. Similarly, the mass of activity by \emph{asymptomatic} and \emph{symptomatic} agents in zone $z$ are
\begin{align*}
e^\Asym_z(\pi,d) & = d[\Asym,z] \: \sum_{a,\mz} a \: \pi[a,\mz \mid \Asym,z], \\ 
e^\Inf_z(\pi,d) & = d[\Inf,z] \: \sum_{a,\mz} a \: \pi[a,\mz \mid \Inf,z].
\end{align*}

In order to consider the event of \ezz{failing to pair with an agent} when the amount of activity in the zone $e_z(\pi,d)$ is \ezz{low}, we introduce a small constant amount $\epsilon > 0$ of fictitious activation that does not belong to any of the agents. Consequently in zone $z$, the probability of not interacting with any agent, the probability of a randomly chosen agent being asymptomatic and the probability of a randomly chosen agent being symptomatic are, respectively,
\begin{align}
\gamma^\emptyset_z(\pi,d) & = 
        \frac{\epsilon}{e_z(\pi,d) + \epsilon}, \\ 
\gamma^\Asym_z(\pi,d) & = \frac{e^\Asym_z(\pi,d)}{e_z(\pi,d) + \epsilon}, \quad  \gamma^\Inf_z(\pi,d) = \frac{e^\Inf_z(\pi,d)}{e_z(\pi,d) + \epsilon}. \label{eq:prob_gammas}
\end{align}
\ezz{Note that for a given $\epsilon$, the probability of encountering an infected agent (symptomatically or not) goes to zero as the amount of infections goes to zero, as desired.}

\input{figures-code/SAIR-state-diagram}

As a result, the probability of a susceptible agent to \emph{not} get infected upon activation \ezz{with degree $a$} is
\begin{multline*}
    %\label{eq:StoS}
    \prob[s^+= \Sus \mid s = \Sus,z,a](\pi,d) \\ =\left(1 - \beta_\Asym \: \gamma^\Asym_z(\pi,d) - \beta_\Inf \: \gamma^\Inf_z(\pi,d)\right)^a.
\end{multline*}
It is easy to see that when a susceptible agent does not interact with any other agent (i.e., $a=0$), it remains susceptible.
%\footnote{We define $0^0=1$ for the edge case if $1 - \beta_\Asym \: \gamma^\Asym_z(\pi,d) - \beta_\Inf \: \gamma^\Inf_z(\pi,d) = 0$.} 
When it participates in exactly one interaction ($a=1$) in $z$, the probability that its neighbor is asymptomatic (respectively, infected) is $\gamma^\Asym_z(\pi,d)$ (respectively, $\gamma^\Inf_z(\pi,d)$). 
When it draws $a>0$ independent agents to interact with, it must not get infected in any of the interactions to remain susceptible, and this occurs with the specified probability. As a consequence, we have
\begin{align*}
& \prob[s^+= \Asym \mid s = \Sus,z,a](\pi,d) = \\
&\quad \qquad 1 - \prob[s^+= \Sus \mid s = \Sus,z,a](\pi,d).
\end{align*}
The remaining transition probabilities follow directly as: 
\allowdisplaybreaks
\begin{align*}
&\prob[s^+= \Inf \mid s = \Asym] = \delta_\Asym^\Inf, \quad \prob[s^+= \Unk \mid s = \Asym] = \delta_\Asym^\Unk, \\
& \prob[s^+= \Asym \mid s = \Asym] = 1 - \delta_\Asym^\Inf - \delta_\Asym^\Unk, \quad \prob[s^+ = \Rec \mid s = \Rec]) = 1 \\
&\prob[s^+= \Rec \mid s = \Inf] = \delta_\Inf^\Rec, \quad \prob[s^+= \Inf \mid s = \Inf] = 1 - \delta_\Inf^\Rec, \\
&\prob[s^+ = \Rec \mid s = \Unk] = \delta_\Unk^\Rec, \quad \prob[s^+ = \Unk \mid s = \Unk] = 1- \delta_\Unk^\Rec
\end{align*}
where we have suppressed $(z,a,\pi,d)$ for better readability. These expressions completely specify $\prob[s^+ \mid s,z,a](\pi,d)$ (Figure~\ref{fig:SAIR_transitions}) and therefore the state transition function~\eqref{eq:Transitions}.

% Notice that if the agent does not activate they will not interact with anyone and will neither infect or get infected. When an agent activates in zone $z$ with degree $a$, $a \: p^{a'}(z)(\pi,d)$ (respectively $a \: p^{i'}(z)(\pi,d)$) is the expected number of asymptomatic (respectively infected) agents they will meet, yielding the transition probability in~\eqref{eq:StoA}. Note that~\eqref{eq:StoA} is a valid probability; letting $\betamax = \max\{\beta_{s,a'}^a,\beta_{s,i'}^a\}$, we have
% \begin{multline}
%     a \: (\beta_{s,a'}^a \: p^{a'}(z)(\pi,d) + \beta_{s,i'}^a \: p^{i'}(z)(\pi,d)) \\\leq \dmax \: \betamax (p^{a'}(z)(\pi,d) + p^{i'}(z)(\pi,d)) \leq \dmax \: \betamax \leq 1.
% \end{multline}


\subsection{Rewards}

Each agent's own state $(s,z)$ and action $(a,\tilde z)$ yields an immediate reward for the agent, composed of a reward $\ract[s,z,a]$ for their activation decision, a reward $\rmig[s,z,\mz]$ for their migration decision, and a reward $\rdis[s]$ for how the agent's health is affected by the disease. Formally,
\begin{equation}
    r[s,z,a,\mz] := \ract[s,z,a] + \rmig[s,z,\mz] + \rdis[s]. \label{eq:Rewards}
\end{equation}

The activation reward is defined as
\[
\ract[s,z,a] := o[a] - c[s, z,a],
\]
where $o[a] \in \mathbb{R}_{+}$ denotes the social benefit of interacting with $a$ other agents and is assumed to be non-decreasing in $a$ with $o[0]=0$, and $c[s,z,a] \in \mathbb{R}_{+}$ denotes the cost imposed by the authority in zone $z$ to discourage activity.
We assume that $c[s,z,a]$ are non-decreasing in $a$ and satisfy
    \[
    \!\!\!c[\Inf,z,a] \ge c[\Sus,z,a] = c[\Asym,z,a] = c[\Unk,z,a] \ge c[\Rec,z,a]
    \]
element-wise, since lockdown measures can be more stringent against individuals showing symptoms and more benign for individuals who are known to be immune.


%\begin{itemize}
%\item Activation yields a nonnegative reward $o[a]$, non-decreasing in $a$, with $o[0]=0$.
    %\item Cost for activation when $s \in \set{\Sus,\Asym}$ (showing no symptoms) is $C^\Asym \in \Real_+^{Z \times \amax}$, where $C^\Asym_{z,a}$ is the cost for activating in zone $z$ with degree $a$, expected to be non-decreasing in $a$. This represents inconveniences imposed by lockdown regulations, which can be different across the zones,
    %\item Zonal lockdown regulation impose an inconvenience on activation (and possibly a practical limit on the maximum number of interactions).  
%\item Cost for activation when $s = \Inf$ (showing symptoms) is similar to above $C^\Inf \in \Real_+^{Z \times \amax}$. One would expect that $C^\Inf \geq C^\Asym$ element-wise, since lockdown measures are typically more stringent against agents showing symptoms,

    %\item Being symptomatic ($s = \Inf$) carries a non-negative cost $\cdis$, representing the illness.
%\end{itemize}
%We obtain
% \begin{align}
%     \ract(s,z,a) = \begin{cases}
%         0 &: a = 0 \text{, } s \neq \Inf, \\
%         -c_D &: a = 0 \text{, } s = \Inf, \\
%         o_a - C^\Asym_{z,a} &: a \neq 0 \text{, } s \in \set{\Sus,\Asym}, \\
%         o_a - C^\Inf_{z,a} - c_D &: a \neq 0 \text{, } s = \Inf, \\
%         o_a &: a \neq 0 \text{, } s = \Rec,
%     \end{cases}
% \end{align}
% \begin{align}
%     \ract(s,z,a) = \begin{cases}
%         o(a) - c(z,a) & \text{if } s \neq \Inf, \\
%         o(a) - c^\Inf(z,a) - c_\text{sym} &\text{if } s = \Inf
%     \end{cases}
% \end{align}

The migration reward encodes the non-negative cost of migrating to a new zone. In this work, we define
\[
    \rmig[s,z,\mz] := \begin{cases}
        0 & \text{if } \mz = z, \\
        -\cmig & \text{if } \mz \neq z.
    \end{cases}
\]
However, one may consider a richer cost function that incorporates specific travel restrictions between zones, etc. 
%and is a function of the infection state (i.e., different cost for symptomatic and immune agents).
The third term in \eqref{eq:Rewards} encodes the cost of being ill:
\[
\rdis[s] := \begin{cases}
- \cdis & \text{if }s = \Inf, \\
0 & \text{otherwise}.
\end{cases}
\]

%derive a model for the strategic decision of an agents that is interested in maximizing the future reward produced by their actions.

\subsection{Agents' Strategic Decisions}

We now introduce the strategic decision-making process of the agents. The immediate expected reward of an agent in state $(s,z)$ when it follows policy $\pi$ is
\begin{equation*}
        R[s,z](\pi) = \sum_{a,\mz} \pi[a,\mz \mid s,z] \: r[s,z,a,\mz],
\end{equation*}
with $r[s,z,a,\mz]$ as defined in~\eqref{eq:Rewards}. The expected discounted infinite horizon reward of an agent in state $(s,z)$ with discount factor $\alpha \in [0,1)$ following the homogeneous policy $\pi$ is recursively defined as
\begin{multline*}
    V[s,z](\pi,d) = R[s,z](\pi) \\+ \alpha \sum_{s^+,z^+} P[s^+,z^+ \mid s,z](\pi,d) \: V[s^+,z^+](\pi,d), %\label{eq:V_full}
\end{multline*}
or, equivalently in vector form,
\begin{equation}
    V(\pi,d) = (I - \alpha \: P(\pi,d))^{-1} \: R(\pi). \label{eq:V-function}
\end{equation}
\ezz{Equation~\eqref{eq:V-function} is the well-known Bellman equation.} Note that \ezz{it} is continuous in the social state $(\pi,d)$, as $I - \alpha \: P(\pi,d)$ is guaranteed to be invertible for $\alpha \in [0,1)$.

%\ezz{\footnote{\ezz{The stochastic matrix $P(\pi,d)$ has largest eigenvalue 1, therefore $I - \alpha \: P(\pi,d)$ cannot have a zero eigenvalue.}}}.

While an agent can compute the expected discounted reward $V(\pi,d)$ at a given social state $(\pi,d)$, the policy $\pi$ may not be optimal for the agent. Thus, we assume that each agent chooses its current action $(a,\mz)$ in order to maximize
\begin{multline}
        Q[s,z,a,\mz](\pi,d) := r[s,z,a,\mz] \\+ \alpha \sum_{s^+,z^+} p[s^+,z^+ \mid s,z,a,\mz](\pi,d) \: V[s^+,z^+](\pi,d),
        \label{eq:definitionQ}
\end{multline}
i.e., the agent is aware of the immediate reward and the effect of its action on their future state; however, it assesses the future reward based on a stationarity assumption on the social state $(\pi,d)$. In other words, the agent chooses its action to maximize a single-stage deviation from the homogeneous policy $\pi$~\cite[Section 2.7]{filar2012competitive}, and assumes that its own actions are not going to affect the social state significantly. 

%This approximation becomes exact at the equilibrium as discussed below.

%Intuitively, this corresponds to assuming that the rest of the population is acting rationally at this moment, and that 