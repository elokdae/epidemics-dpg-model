\section{Introduction}

%and authorities take various measures to prevent its spread. These measures include restricting large gatherings and travel, imposing lock-downs, and others. Furthermore, individuals %epidemic mitigation strategies based on centralized resource allocation \cite{preciado2014optimal,hota2020closed} as well as 
%Therefore, it is critical to rigorously investigate the interplay between individual behavior, emerging network structure and epidemic evolution. 
%Mathematical modeling of epidemic evolution on networks has a rich history; see \cite{nowzari2016analysis,pastor2015epidemic, pare2020arc} for comprehensive reviews. 

Infectious diseases or epidemics spread through society by exploiting social interactions. As the disease becomes more prevalent, individuals reduce their social interaction and even migrate to safer locations in a strategic and non-myopic manner \cite{Bloomberg, Brookings}, which plays a significant role in epidemic evolution. Accordingly, past work has explored decentralized or game-theoretic protection strategies against epidemics on (static) networks \cite{trajanovski2015decentralized,hota2019game,paarporn2017networked,eksin2016disease,huang2019differential}. More recently, evolution of network topology and epidemic states in a comparable time-scale have been studied in the framework of activity-driven networks \cite{zino2017analytical,zino2020analysis}. Game-theoretic decision-making in this framework was recently studied in \cite{hota2020impacts} where myopic bounded rational agents decide whether to activate or not as a function of the epidemic prevalence.

To the best of our knowledge, there have been few rigorous game-theoretic formulations that model agents that
\begin{itemize}
\item decide their degree of activation, and consequently influence the resulting network topology,
\item decide whether to migrate to different locations, and
\item maximize both current and long-run future pay-off
\end{itemize}
in the same time-scale as epidemic evolution. In this work, we present a framework to address the above research gap.

%%%%%%%%%%%%%%%%%%%%

Motivated by the presence of asymptomatic carriers in COVID-19 \cite{hu2020clinical, pang2020public}, we build upon the SAIR epidemic model studied in \cite{ansumali2020modelling,chisholm2018implications}. We consider a large population regime where the state of an individual agent is characterized by its infection state and its location (or zone). At discrete time instants, each agent decides its degree of activation and its next location. The agent is then paired randomly with other agents, and its infection state evolves following an augmented SAIR epidemic model which also takes into account \emph{unknowingly recovered} agents as described in Section \ref{sec:model}. Agents maximize a discounted infinite horizon expected reward which is a function of the aggregate infection state, the zonal distribution of the agents, and the policy followed by the population. 

%Previous works that study dynamic strategic interactions in large populations include anonymous sequential games~\cite{jovanovic1988anonymous,weintraub2005oblivious,adlakha2015equilibria} and mean field games~\cite{huang2006large,lasry2007mean,gomes2010discrete,doncel2016mean,neumann2020stationary}, which are often difficult to apply in practical models.

In a departure from the conventional assumption of a static population distribution in the classical population game setting \cite{sandholm2010population}, epidemic evolution leads to a dynamically evolving population which makes the analysis challenging. We utilize the recently developed framework of \emph{dynamic population games}~\cite{elokda2021dynamic}, in which the authors show a reduction of the dynamic setting to a static population game setting~\cite{sandholm2010population}. This simplifies the analysis in comparison to the existing approaches such as anonymous sequential games~\cite{jovanovic1988anonymous,adlakha2015equilibria} and mean field games~\cite{huang2006large,gomes2010discrete,neumann2020stationary}, and is particularly useful for epidemic models. As a consequence of the reduction, standard evolutionary models~\cite{sandholm2010population} can be adapted for the coupled dynamics of the agents' states and strategic decision making, which evolve on the same time-scale.

The paper is structured as follows: the dynamic population model is presented in Section~\ref{sec:model}, and its stationary equilibria are analyzed in Section~\ref{sec:equilibrium}. The evolutionary update of agents' policies is modeled in Section~\ref{sec:socdynamics}. Numerical experiments reported in Section~\ref{sec:simulations} provide compelling insights into agents' behavior, effects of lockdown measures and strategic mobility patterns. For instance, we observe that if recovered agents are exempt from lockdown measures, then an increased level of activity by susceptible and asymptomatic agents can happen without having much impact on the peak and total infections. Their strategic behavior does not lead to a higher infection level and the social welfare improves due to overall higher activity levels.



% \begin{table}[!h]
% \caption{Glossary of symbols}
% \label{table_example}
% \centering
% % Some packages, such as MDW tools, offer better commands for making tables
% % than the plain LaTeX2e tabular which is used here.
% \begin{tabular}{@{}ll@{}}
% %\hline\hline\noalign{\smallskip}
% \toprule
% Symbol & Description \\
% %\noalign{\smallskip}\hline\noalign{\smallskip}
% \midrule
% $S$ & Susceptible (not infected) \\\noalign{\smallskip}
% $A$ & Infected and asymptomatic \\\noalign{\smallskip}
% $I$ & Infected and symptomatic \\\noalign{\smallskip}
% $R$ & Recovered \\\noalign{\smallskip}
% $\St = \set{S,A,I,R}$ & Set of infection states \\\noalign{\smallskip}
% $s \in \St$ & Infection state of an agent \\\noalign{\smallskip}
% $\Z = \set{1,\dots,Z}$ & Set of zones \\\noalign{\smallskip}
% $z \in \Z$ & Current zone of an agent \\\noalign{\smallskip}
% $\X = \St \times \Z$ & Set of states \\\noalign{\smallskip}
% $x=(s,z) \in \X$ & State of an agent \\\noalign{\smallskip}
% $\Delta(\cdot)$ & Probability simplex over elements of set $\cdot$ \\\noalign{\smallskip}
% $\D = \Delta(\X)$ & Set of state distributions \\\noalign{\smallskip}
% $d \in \D$ & Distribution of agents' states \\\noalign{\smallskip}
% $d^0 \in \D$ & Initial state distribution \\\noalign{\smallskip}
% $\amax$ & Maximum activation degree of an agent \\\noalign{\smallskip}
% $\A = \set{0,1,\dots,\amax}$ & Set of activation degrees \\\noalign{\smallskip}
% $a \in \A$ & Activation degree of an agent \\\noalign{\smallskip}
% $\mz \in \Z$ & Migration target zone of an agent \\\noalign{\smallskip}
% $\U = \A \times \Z$ & Set of actions \\\noalign{\smallskip}
% $u=(a,\mz) \in \U$ & Action of an agent \\\noalign{\smallskip}
% $\pi : \X \rightarrow \Delta(\U)$ & Symmetric policy of agents \\\noalign{\smallskip}
% $\pi(\cdot \mid s,z) \in \Delta(\U)$ & Distribution of agents' actions in state $(s,z)$ \\\noalign{\smallskip}
% $\Pi$ & Set of policies \\\noalign{\smallskip}
% $(\pi,d) \in \Pi \times \D$ & Social state \\\noalign{\smallskip}
% $j$ & Superscript denoting a neighbor quantity \\\noalign{\smallskip}
% $\beta_{S,A^j}^A$ & \makecell[l]{Probability of infection ($S \rightarrow A$) upon \\ interacting with asymptomatic neighbor ($A^j$)} \\\noalign{\smallskip}
% $\beta_{S,I^j}^A$ & \makecell[l]{Probability of infection ($S \rightarrow A$) upon \\ interacting with symptomatic neighbor ($I^j$)} \\\noalign{\smallskip}
% $\dots$ \\
% %\noalign{\smallskip}\hline\hline
% \bottomrule
% \end{tabular}
% \end{table}