\section{Numerical Case Studies}
\label{sec:simulations}

We present a select number of case studies to showcase 
\begin{itemize}
    \item the effect of agents' strategic activation decisions on the spread of the epidemic, 
    \item the impact of lockdown measures on both epidemic containment and social welfare, and
    \item the effect of strategic migration decisions on how the epidemic spreads across multiple locations.
\end{itemize}

%how our model can be insightful for the analysis of different phenomena in epidemic spreading:

For this purpose, we consider an infectious epidemic characterized by $\beta_\Asym = \beta_\Inf = 0.2$, $\delta_\Asym^\Inf = \delta_\Asym^\Unk = 0.08$, and $\delta_\Inf^\Rec=0.04$.
The agents can activate up to degree $\amax=6$, and the activation reward is linear in the activation degree, with a unit reward for maximum activation $o[\amax] = 1$.
The illness is quite severe, with a discomfort cost $\cdis = 10$.
% It is also discomforting to activate while ill, and therefore the cost of activation dominates the reward, with $c[\Inf,z,a]=3\:o[a]$ for all zones.
Initially, we let the agents choose an activation degree uniformly at random and do not plan any move. The agents are highly rational ($\lambda=10$) and unless otherwise stated, we consider that they update their decisions with an inertia $\eta=0.2$. We consider both a single zone and a two zone setting, and denote the zones by $\zuno$ and $\ztwo$.
In all cases, the epidemic starts in $\zuno$ with a proportion of $2\%$ of that zone's population asymptomatic ($\Asym$), and $1\%$ infected ($\Inf$).

We further consider that authorities can enforce lockdown regulations through the parameter $\alock[s,z]$, which represents the \emph{maximum allowed activation degree} and can differ between zones and for agents in different infection states.
Lockdown is implemented by setting $c[s,z,a] = 0$ if $a \leq \alock[s,z]$, and $c[s,z,a] = 3\:o[a]$ otherwise.
Regardless of the lockdown measures, we always assume that the discomfort of the illness is sufficient to prevent symptomatically infected agents from activating. As a consequence, the main threat of the epidemic is due to the presence of asymptomatically infected agents in the population.

\subsection{Strategic activation under lockdown measures}

We first investigate the single zone scenario, with a focus on how agents of different cognitive ability react under various lockdown measures, and the resulting effects on the epidemic spread.
Figure~\ref{fig:lockdown-trends} shows an example with lockdown degree $\alock=2$ and three cases.
In cases~\eqref{fig:a_lock2-alpha0-delta_U_R0-R-lock} and~\eqref{fig:a_lock2-alpha0.9-delta_U_R0-R-lock}, the lockdown is enforced on the whole population, and the cases differ in the agents' cognitive level of the future.
In~\eqref{fig:a_lock2-alpha0-delta_U_R0-R-lock}, the agents are completely myopic.
Notice how they simply adhere to the lockdown degree\footnote{\label{ft:pbr}A slight deviation to the expected degree of activation is due to the agents' bounded rationality.}, which aligns with standard epidemic models.
Farsighted agents~\eqref{fig:a_lock2-alpha0.9-delta_U_R0-R-lock}, on the other hand, actively adjust their activation decisions in response to the epidemic threat, and volunteer to limit their activity beyond the lockdown requirement at peak infection times. The reduction in activity levels leads to a less severe epidemic spread, with a smaller total and peak infection.

%This aligns with the intuitive notion of individuals \emph{acting responsibly} under the epidemic threat, and our model demonstrates that this is indeed in their best interest.
%But this is not the only reason why welfare increases; the activity of immune agents also helps susceptible agents, by reducing the probability of infection and encouraging them to activate more.
%The reduced probability of infection caused by the increased activity of immune agents encourages them to activate more.
%This accounts to why the measures do not significantly impact the peak of infections; the increased level of activity counteracts the decreased risk of infection.
%It is remarkable, however, that the same peak is maintained with an increased level of activity, and that the total infections see a significant improvement as well.


%In regards to the isolated effect of performing serological testing, it results in a decrease of the total infections from $53.2\%$ to $46.2\%$ in the example case of $\alock=2$.

\input{figures-code/lockdown-trends}

\input{figures-code/a-lock-performance}

Case~\eqref{fig:a_lock2-alpha0.9-delta_U_R0} also considers farsighted agents, but this time \emph{recovered} agents are exempt from lockdown and thus activate at the maximum degree, as per their dominant strategy.
This leads to a significant reduction of the total amount of infections. In fact, due the prevalence of activity of immune agents, it becomes less likely for a susceptible agent to encounter an infected agent. Consequently, susceptible, asymptomatic and unknowingly recovered agents too increase their level of activation. Nevertheless, the peak infection remains being largely unchanged.

This insight is further explored in Figure~\ref{fig:lockdown-performance}, which shows the effect of different lockdown measures on three main performance indices:
the \emph{total infections} ($\Rec+\Unk$ at the end of the epidemic), 
the \emph{peak infections} (highest value of $\Inf$\footnote{We do not consider infected but asymptomatic agents since they do not contribute to the load on medical facilities.}),
and the \emph{average welfare} (mean reward~\eqref{eq:Rewards} in the population over the duration of the epidemic).
%By \emph{total infections} we denote the proportion of recovered agents (knowingly or unknowingly) at the end of the epidemic, by \emph{peak infections} the peak proportion of infected and symptomatic ($\Inf$) agents\footnote{We do not consider infected but asymptomatic agents since they do not contribute to the load on medical facilities.}, and by \emph{average welfare} the mean reward~\eqref{eq:Rewards} observed in the population over the duration of the epidemic.
We depict the same cases considered in Figure~\ref{fig:lockdown-trends} and perform a parameter sweep over the strictness of the lockdown $\alock$.
Additionally, we showcase the effect of performing \emph{serological tests} to increase the amount of knowingly immune agents, at rate $\delta_\Unk^\Rec=0.05$.
We observe that myopic agents perform poorly along all the performance metrics. 
For farsighted agents, the exemption of recovered agents and serological tests lead to significant improvements in the average welfare because the knowingly immune agents are able to achieve their maximum activation degree without increasing the threat of the epidemic.

%On the contrary, as discussed before, these measures reduce the probability of infection and ultimately improve the final outcome (total infections).
% \begin{figure*}[h]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.00_R_lock_d.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0-R-lock-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.00_d.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.40_d.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0.4-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.90_delta_U_R_0.00_R_lock_d.pdf}
%         \label{fig:a_lock2-alpha0.9-delta_U_R0-R-lock-d}
%     }
    
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.00_R_lock_mean_a.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0-R-lock-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.00_mean_a.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.00_delta_U_R_0.40_mean_a.pdf}
%         \label{fig:a_lock2-alpha0-delta_U_R0.4-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_2_alpha_0.90_delta_U_R_0.00_R_lock_mean_a.pdf}
%         \label{fig:a_lock2-alpha0.9-delta_U_R0-R-lock-mean_a}
%     }
%     \caption{Dashed is the lockdown activation degree. \textbf{(Left)} is when everyone is under lockdown, \textbf{(column-2)} is when $\Rec$ are released from lockdown, and \textbf{(column-3)} is when additionally serological tests are performed (with $\delta_\Unk^\Rec=0.4$). The combined effect of 2-3 reduces both the peak and the total number of infections because recovered agents activate at max degree (Proposition~\ref{prop:Rec}), which reduces the risk of infections through the $\gamma$ terms. Serological tests help with that because they shift the weight of $\Unk$ agents, who do not activate much, to $\Rec$, and improve the herd immunity. \textbf{(Right)} is when agents are farsighted. The agents actively manage the risk of the epidemic by strategically choosing to activate less than lockdown enforces. This leads to to a less severe outbreak, both in terms of the peak and the total amount of infections.}
%     \label{fig:lockdown-trends}
% \end{figure*}



% \begin{figure}[bt]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_lockdown_d_S.pdf}
%         \label{fig:lockdown-d_S}
%     }
    
%     \subfloat{
%         \includegraphics{tikz/tikz_lockdown_max_d_I.pdf}
%         \label{fig:lockdown-max_d_I}
%     }
%     \caption{Our model allows us to systematically investigate different measures to contain the epidemic. Can improve things for myopic agents by releasing $\Rec$ from lockdown and performing serological tests. If agents are farsighted, they largely manage the risk of the epidemic on their own through their strategic behavior.}
%     \label{fig:lockdown-performance}
% \end{figure}

% \begin{figure*}[h]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_1_alpha_0.00_delta_U_R_0.00_R_lock_d.pdf}
%         \label{fig:alpha0-delta_U_R0-R-lock-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_1_alpha_0.00_delta_U_R_0.00_d.pdf}
%         \label{fig:alpha0-delta_U_R0-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_3_alpha_0.90_delta_U_R_0.00_R_lock_d.pdf}
%         \label{fig:alpha0.9-delta_U_R0-R-lock-d}
%     }
    
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_1_alpha_0.00_delta_U_R_0.00_R_lock_mean_a.pdf}
%         \label{fig:alpha0-delta_U_R0-R-lock-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_1_alpha_0.00_delta_U_R_0.00_mean_a.pdf}
%         \label{fig:alpha0-delta_U_R0-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_a_lock_3_alpha_0.90_delta_U_R_0.00_R_lock_mean_a.pdf}
%         \label{fig:alpha0.9-delta_U_R0-R-lock-mean_a}
%     }
%     \caption{Dashed is the lockdown activation degree. \textbf{(Left)} is when everyone is under lockdown. \textbf{(Center)} is when $\Rec$ are released from lockdown. This reduces both the peak and the total number of infections because they activate at max degree (Proposition~\ref{prop:Rec}), which reduces the risk of infections through the $\gamma$ terms. Performing serological tests will achieve a similar effect because it will shift the weight of $\Unk$ agents, who do not activate much, to $\Rec$, and improve the herd immunity. \textbf{(Right)} is when agents are farsighted. Here the lockdown is looser (3 instead of 1), and yet the agents manage the risk of the epidemic by strategically choosing to activate less. This leads to a similar peak to the myopic case with stricter lockdown. However, the total number of infections is bad, because the agents go back to activating once the risk decreases. In order to control the total number of infections, it appears that strict lockdown is necessary.}
%     \label{fig:lockdown-trends}
% \end{figure*}

\subsection{Strategic migration}

We showcase the effect of strategic migration in a setting with two zones, with zone $\zuno$ initially holding $90\%$ of the total population, and zone $\ztwo$ initially infection free.
In both zones, (knowingly) recovered agents are exempt from lockdown, and the lockdown restrictions for the other agents are different in the zones. Namely, $\zuno$ has a looser lockdown, with a maximum allowed activation degree of 4, whereas $\ztwo$ only allows a maximum of 2. The migration cost is $\cmig=2$, and the agents are farsighted with $\alpha=0.9$.
Note that with these parameters, susceptible agents will want to move to $\zuno$ when the epidemic is not prevalent, as per Proposition~\ref{pro:characterizationeq}\footnote{Here, $\rstar[\Sus,\ztwo]=\frac{1}{3}$ and $\rbar[\Sus]=\frac{2}{3}$, therefore $\ztwo \in \Z^0_\Sus$.}.
The inertia in the policy update is $\eta=0.1$.

%\ezz{This represents that $\ztwo$ has a more precautionary strategy in combating the epidemic than $\zuno$.}

Additionally, both zones perform serological testing at rate $\delta_\Unk^\Rec=0.01$.
The resulting epidemic spread is displayed in Figure~\ref{fig:migration-trends}.
First, notice how the proportion of unknowingly recovered agents decays, in contrast to Figure~\ref{fig:lockdown-trends}, in which no serological testing is performed.

We now focus on the strategic migration behavior of agents who are either susceptible or think they are ($\Sus$, $\Asym$, $\Unk$). Note that symptomatically infected and recovered agents never move since they are immune to the threat of the epidemic, and the activation costs are the same for them in both zones.
Initially, the epidemic risk is still small in $\zuno$, and the occupants of $\ztwo$ start moving there to benefit from the more lenient lockdown measure.
This trend soon reverses, however, with the rise of infections in $\zuno$: the strategic agents elect to move to the zone with stricter lockdown to escape the epidemic risk.
Since a proportion of the movers are asymptomatically infected, this leads to an outbreak of the epidemic in $\ztwo$ as well, with lower, but significant, peak infections than $\zuno$.
Eventually, once the infections in $\zuno$ has decreased sufficiently (at approximately day 50), some $\ztwo$ residents move to $\zuno$ again, initiating a \emph{second wave of infections} in $\zuno$. At the end of the epidemic, all the remaining agents move from $\ztwo$ as per their dominant strategy. 

%The activation degrees of the agents in both the zones are also shown in Figure~\ref{fig:migration-trends}.

%This disables the moves from $\ztwo$ for a period of time.

\input{figures-code/migration-trends}

% \subsection{Epidemic in two zones under no lockdown restrictions}

% We first consider a scenario where there are two zones of living. Zone 1 (denoted $z_1$) initially holds the majority of the population, and it is where the epidemic outbreak occurs. Zone 2 (denoted $z_2$) is initially infection-free. We utilize our model to shed insight on how the epidemic will spread when no lockdown restrictions are enforced. In particular, we would like to investigate the following questions:
% \begin{itemize}
%     \item Will future sighted agents limit their activity on their own, out of the fear of a future illness?
%     \item How does the epidemic risk affect the strategic migration between the two zones?
%     \item How do the strategic migrations affect the distribution of the epidemic spread across the two zones?
% \end{itemize}

% For this purpose, we consider a SAIR epidemic that is characterized by $\beta_\Asym = \beta_\Inf = 0.2$, $\delta_\Asym^\Inf = 0.2$, and $\delta_\Asym^\Rec = \delta_\Inf^\Rec=0.04$. We consider that agents can activate up to degree $\amax=3$, and the activation reward is linearly increasing in the activation degree, with a unit reward for maximum activation $o_\amax = 1$. The decease is quite severe, costing 10 times the maximum activation reward per day of illness ($c_D = 10$). Moving is also costly with respect to activation, with $c_M=5$.

% We adapt the perturbed best response dynamics for the agents' decision making model, and discretize the corresponding evolutionary policy-state dynamics~\eqref{eq:PolEvolPBR}--\eqref{eq:StateEvolPBR} for the purpose of simulation, with a discrete time step of 0.01. The policy update rate is set to $\eta=0.3$, representing a delay in the reaction of the agents. We consider two values for the agents' bounded rationality, one where they are highly rational ($\lambdahigh = 10$) and one where they are less rational ($\lambdalow=1$).

% \input{figures_code/lambda_alpha_performance}

% \input{figures_code/lambda_alpha_trends}

% \begin{figure*}[bt]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_alpha_d_up_S.pdf}
%         \label{fig:lambda-alpha-d_S}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_alpha_max_d_up_I.pdf}
%         \label{fig:lambda-alpha-max_d_I}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_alpha_tot_move.pdf}
%         \label{fig:lambda-alpha-tot_move}
%     }
%     \caption{A short caption}
%     %Our model suggests that the epidemic's severity reduces when the agents are highly rational and farsighted. This is in part due to the agents' strategic migrations. Displayed \textbf{left} is the total proportion of non-infected agents at the end of the epidemic, which improves with higher values of the bounded rationality $\lambda$ and the future discount factor $\alpha$. The sharp drop for $\lambdahigh$ at $\alpha \approx 0.625$ occurs when a small proportion of asymptomatic agents strategically migrate to the initially infection-free Zone 2 and spread the epidemic there. Displayed \textbf{center} is the peak of the infected state curve ($\Inf$) in the two zones. This is minimized at $\lambdahigh$ and a value of $\alpha$ that leads the agents to migrate just enough to equalize the peak of the infections in both zones ($\alpha \approx 0.875$). Displayed \textbf{right} is the total amount of moves from the incumbent Zone 1 to the initially infection-free Zone 2. Since moving is immediately costly, highly rational agents must be highly farsighted to move, whereas agents with low rationality move at a small constant rate for low values of $\alpha$. This leads to an equalization effect of the epidemic spread across both zones, and lifts some of the load on the incumbent Zone 1.
%     \label{fig:lambda-alpha-performance}
% \end{figure*}

% \begin{figure*}[t]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_1_alpha_0.75_d.pdf}
%         \label{fig:lambda1-alpha0.75-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.75_d.pdf}
%         \label{fig:lambda10-alpha0.75-d}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.9_d.pdf}
%         \label{fig:lambda10-alpha0.9-d}
%     }
    
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_1_alpha_0.75_p_m.pdf}
%         \label{fig:lambda1-alpha0.75-p_m}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.75_p_m.pdf}
%         \label{fig:lambda10-alpha0.75-p_m}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.9_p_m.pdf}
%         \label{fig:lambda10-alpha0.9-p_m}
%     }
    
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_1_alpha_0.75_mean_a.pdf}
%         \label{fig:lambda1-alpha0.75-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.75_mean_a.pdf}
%         \label{fig:lambda10-alpha0.75-mean_a}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_lambda_10_alpha_0.9_mean_a.pdf}
%         \label{fig:lambda10-alpha0.9-mean_a}
%     }
%     \caption{Evolution of the epidemic spread (\textbf{top}), the agents' migration decisions (\textbf{middle}), and the agents' activation decisions (\textbf{bottom}) for select values of the bounded rationality $\lambda$ and the future discount factor $\alpha$. We focus on agents in infection states $\Asym,\Inf,\Rec$. At the low bounded rationality $\lambda=1$ and future discount factor $\alpha=0.75$ (\textbf{left}), all agents move with a small constant probability, leading to an early spread of the epidemic to the initially infection-free Zone 2, and a decreased load on the incumbent Zone 1. In contrast, at the high bounded rationality $\lambda=10$ and the low future discount factor $\alpha=0.75$ (\textbf{center}), none of the agents move, with the exception of a minute fraction of asymptomatic agents at the beginning of the epidemic. As a consequence, the incumbent Zone 1 contains the epidemic largely on its own, and the outbreak at Zone 2 occurs after some delay. A converse trend is observed at the high bounded rationality $\lambda=10$ and future discount factor $\alpha=0.9$ (\textbf{right}), where a significant amount of strategic migrations of asymptomatic agents at the beginning of the epidemic leads to it spreading more considerably in the initially infection-free Zone 2. Nevertheless, the overall severity of the epidemic is reduced at $\alpha=0.9$, due to both that is splits over the two zones, and also the reduced activation of asymptomatic agents at the beginning of the epidemic.}
%     \label{fig:lambda-alpha-trends}
% \end{figure*}

% \subsection{Epidemic under lockdown restrictions}

% We now investigate the effect of enforcing lockdown restrictions by varying the activation cost $C^\Asym$...

% \begin{figure*}[bt]
%     \centering
%     \subfloat{
%         \includegraphics{tikz/tikz_C_A_d_S.pdf}
%         \label{fig:C_A-d_S}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_C_A_max_d_I.pdf}
%         \label{fig:C_A-max_d_I}
%     }
%     \hfil
%     \subfloat{
%         \includegraphics{tikz/tikz_C_A_welfare.pdf}
%         \label{fig:C_A-tot_move}
%     }
%     \caption{Something along the lines of strict lockdown restrictions reduce the severity of the epidemic, but at some point it hurts the economy too much}
%     \label{fig:C_A-performance}
% \end{figure*}