\section{Social State Dynamics} 
\label{sec:socdynamics}

Different stationary equilibria correspond to drastically different outcomes in terms of the impact of the epidemic on the population.
For this reason, we investigate the \emph{transient behavior} leading to the equilibrium, which is determined by the state dynamics from Section~\ref{ssec:statetransitions}, i.e., $d^+ = d \: P(\pi,d)$, and the way in which agents update their policies. For this second part, we get inspiration from the \emph{evolutionary dynamic} models in classical population games~\cite{sandholm2010population}, and more precisely from the \emph{perturbed best response dynamics}.

% \subsection{State dynamics}

% Recall that the individual agent state dynamics under the symmetrical policy $\pi$ are characterized by the Markov chain with stochastic matrix $P(\pi,d)$. We assume that the discrete time instances of the individual agents (corresponding to their activation/migration opportunities) occur at rings of independent Poisson alarm clocks with unit rate. This lets us consolidate the individual state dynamics into the following continuous-time model for the macroscopic evolution of state distribution $d$~[DPG-CITE]
% \begin{align}
%     \dot{d}(s,z) = \sum_{s^-,z^-} d(s^-,z^-) \: P(s,z \mid s^-,z^-)(\pi,d) - d(s,z),
% \end{align}
% which in vector form is
% \begin{align}
%     \dot{d} &= d \: P(\pi,d) - d \\
%     &:= W(\pi,d). \label{eq:StateDynamics}
% \end{align}
% The dynamics~\eqref{eq:StateDynamics} are initialized at the initial state distribution parameter $d^0$.

% Note that the rest points of the societal state dynamics are characterized by $d = d \: P(\pi,d)$, which is the familiar stationary distribution condition of the Markov chain with stochastic matrix $P(\pi,d)$. However, the stationary distribution(s) cannot be computed via the classical eigenvector approach, since the stochastic matrix is itself parametrized by the state distribution\footnote{Recall that the stationary distribution(s) of a time-homogeneous Markov chain with stochastic matrix $P$ coincide with the left eigenvectors of $P$ corresponding to eigenvalue 1. However, since in our setting $P$ is a function of $d$, the left eigenvectors of $P(\pi,d)$ corresponding to eigenvalue 1 need not be $d$.}.

%\subsection{Evolutionary Policy Dynamics}
%\label{sec:dynamics}

% A continuous time model of the evolution of the symmetric policy $\pi$ is
% %and state distribution $d$ is
% \begin{align}
%     \dot{\pi}(\cdot \mid s,z) &=  \eta \: H(Q(s,z,\cdot)(\pi,d),\pi(\cdot \mid s,z)), \; : s \neq \Asym, \label{eq:PolEvol} %\\
% %    \dot{d} &= W(\pi,d),
% \end{align}
% and $\pi(\cdot \mid \Asym,z) = \pi(\cdot \mid \Sus,z)$. Here, $H : \Real^{|\A|} \times \Delta(\A) \rightarrow \Real^{|\A|}$ is one of the familiar \emph{evolutionary dynamics} in classical population games~\cite{sandholm2010population}, and~\eqref{eq:PolEvol} describes the evolution of the action distribution of agents of state $(s,z)$. The evolution of the state distribution $d$ is as in~\eqref{eq:StateDynamics}. 

% The parameter $\eta \in (0, 1]$ controls the rate of policy changes with respect to the rate of social interactions; for $\eta < 1$, agents have \emph{inertia} in their decision making, i.e., policy changes occur at a slower time scale than interactions, and for $\eta = 1$, agents are making their action decisions as they interact (same time scale).
%

We assume that the agents are not perfectly rational, with the bounded rationality factor $\lambda \in [0,\infty)$. When they are making a decision on which action to play, they follow the \emph{logit choice} function~\cite[Section~6.2]{sandholm2010population}, given by
\[
    \pbr[a,\mz \mid s,z](\pi,d) = \frac{\exp{(\lambda \: Q[s,z,a,\mz](\pi,d))}}{\sum_{a',\mz'} \exp{(\lambda \: Q[s,z,a',\mz'](\pi,d))}}.
\]
%Note that $\tilde{\pi}[a,\mz \mid s,z](\pi,d) > 0$ and $\sum_{a,\mz}\tilde{\pi}[a,\mz \mid s,z](\pi,d) = 1$, so $\tilde{\pi}[\cdot \mid s,z](\pi,d)$ is a probability distribution over the actions $(a,\mz)$. 
For $\lambda = 0$, it results in a uniform distribution over all the actions.
%, representing agents that simply pick actions at random.
At the limit $\lambda \rightarrow \infty$, we recover the perfect best response.
At finite value of $\lambda$, it assigns higher probabilities to actions with higher payoffs. 

%modeling the effect of concurrent reasons (unrelated to the epidemic) that affect the agents' strategic behavior.
%, with a uniform probability distribution placed on the action(s) achieving the highest payoff. 
%The resulting evolutionary dynamic is
% \begin{multline}
%     H(Q(s,z,\cdot)(\pi,d),\pi(\cdot \mid s,z)) \\
%     = \pbr(\cdot \mid s,z)(\pi,d) - \pi(\cdot \mid s,z)(\pi,d).
% \end{multline}

% The combined policy-state distribution dynamics are
% \begin{align}
%     \dot{\pi}(\cdot \mid s,z) &=  \eta \: \left(\pbr(\cdot \mid s,z)(\pi,d) - \pi(\cdot \mid s,z)(\pi,d)\right), \; : s \neq \Asym, \label{eq:PolEvolPBR}\\
%     \dot{d} &= d \: P(\pi,d) - d, \label{eq:StateEvolPBR}
% \end{align}
% and $\pi(\cdot \mid \Asym,z) = \pi(\cdot \mid \Sus,z)$.

In order to model the fact that agents update their policies gradually, we consider the discrete-time update 
\[
    \pi^+[\cdot \mid s,z] = (1 - \eta) \: \pi[\cdot \mid s,z] + \eta \: \pbr[\cdot \mid s,z],
\]
where $\eta \in (0,1]$ is a parameter that controls the rate of policy change: for $\eta < 1$, agents have \emph{inertia} in their decision making, while for $\eta = 1$ agents promptly update their action decision to the perturbed best response $\pbr$.
Note that this update model leads to a perturbed version of the equilibrium policy $\epi$ at the rest points, rather than the exact policy~\cite{sandholm2010population}.