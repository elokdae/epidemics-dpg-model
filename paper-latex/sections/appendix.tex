\appendix

% \subsection*{Proof of Proposition~\ref{prop:StatAI}}

% The main intuition is that states $\Asym$ (asymptomatic) and $\Inf$ (infected) are \emph{transient} in the infection state Markov chain (see Figure~\ref{fig:SAIR_transitions}), and evolve independently of the agents' migrations. If $\delta_\Unk^\Rec>0$, state $\Unk$ (unknowingly recovered) is transient as well.

% Formally, we first consider the stationary proportion of \emph{recovered} agents ($\Rec$) in an arbitrary zone $z$. Writing the stationarity condition~\eqref{eq:SE-2} explicitly for this state yields
% \begin{multline}
%     \sd[\Rec,z] = \sum_{s',z'} \sd[s',z'] \: P[\Rec,z \mid s',z'](\epi,\sd) \\
%     = \sum_{s',z'} \sd[s',z'] \: \sum_{a,\mz} \epi[a,\mz \mid s',z'] \: p[\Rec,z \mid s',z',a,\mz](\pi,d) \\
%     = \delta_\Inf^\Rec \sum_{z'} \sd[\Inf,z'] \sum_{a,\mz} \epi[a,\mz \mid \Inf, z'] \: \prob[z \mid z', \mz] \\
%     + \delta_\Unk^\Rec \sum_{z'} \sd[\Unk,z'] \sum_{a,\mz} \epi[a,\mz \mid \Unk, z'] \: \prob[z \mid z', \mz] \\
%     + \sum_{z'} \sd[\Rec,z'] \sum_{a,\mz} \epi[a,\mz \mid  \Rec, z'] \: \prob[z \mid z', \mz], \label{eq:StatR}
% \end{multline}
% which follows from our transition model~\eqref{eq:stochasticMatrix}--\eqref{eq:Transitions}. The only way to arrive in state $(s=\Rec,z)$ after a one-step transition is for the agents to move to (or stay in) zone $z$ and recover if they were infected (with probability $\delta_\Inf^\Rec$), learn that they are recovered if they were unknowingly recovered (with probability $\delta_\Unk^\Rec$), or if they were already recovered.
% Summing~\eqref{eq:StatR} over all zones $z \in \Z$
% leads to
% \[
%     \sum_{z} \sd[\Rec,z] = \delta_\Inf^\Rec \sum_{z'} \sd[\Inf,z'] + \delta_\Unk^\Rec \sum_{z'} \sd[\Unk,z'] + \sum_{z'} \sd[\Rec,z'],
% \]
% which can be written as
% \begin{equation}
%     \delta_\Inf^\Rec \sum_z \sd[\Inf,z] + \delta_\Unk^\Rec \sum_z \sd[\Unk,z] = 0. \label{eq:StatCondition}
% \end{equation}

% Since all terms in~\eqref{eq:StatCondition} are non-negative and $\delta_\Inf^\Rec > 0$, it must hold that $\sd[\Inf,z] = 0$ for all $z \in \Z$. If additionally, $\delta_\Unk^\Rec > 0$, it must also hold that $\sd[\Unk,z] = 0$ for all $z \in \Z$. So there are no infected agents at the stationary distribution, and no unknowingly recovered agents if $\delta_\Unk^\Rec>0$.

% It remains to show that there are no asymptomatic agents as well. For this purpose, we consider the stationary proportion of infected agents in zone $z$, which is zero as just demonstrated. In a manner similar to~\eqref{eq:StatR}, the stationarity condition~\eqref{eq:SE-2} for this state is
% \begin{multline*}
%     \sd[\Inf,z] 
%     = \delta_\Asym^\Inf \sum_{z'} \sd[\Asym,z'] \sum_{a,\mz} \epi[a,\mz \mid \Inf, z'] \: \prob[z \mid z', \mz] \\
%     + (1 - \delta_\Inf^\Rec) \sum_{z'} \sd[\Inf,z'] \sum_{a,\mz} \epi[a,\mz \mid \Unk, z'] \: \prob[z \mid z', \mz].
% \end{multline*}
% Summing over all $z \in \Z$ and using $\sd[\Inf,z]=0$ yields
% $$ \delta_\Asym^\Inf \sum_z \sd[\Asym,z] = 0.$$
% Since $\delta_\Asym^\Inf>0$, we must have $\sd[\Asym,z]=0$ for all $z$. 


\subsection*{Proof of Proposition~\ref{pro:characterizationeq}}

The first property follows the fact that states $\Asym$, $\Inf$, and  $\Unk$  are \emph{transient} in the infection state Markov chain (see Figure~\ref{fig:SAIR_transitions}), and evolve independently of the agents' migrations. We omit the formal steps, \ezz{which follow standard Markov chain theory arguments}.

We now observe that, given a distribution that is only supported on the two compartments $\Sus$ and $\Rec$, no transition between compartments of the extended SAIR model is possible. The reward for agents in both these compartments then becomes independent from the actions of others, and the set $\mathcal A^*_{s,z}$ defines their \emph{dominant activation strategies}. 

We now consider migration strategies of the agents at the equilibrium $(\epi,\sd)$. We first consider an agent in infection state $s \in \{\Sus,\Rec\}$ residing in zone $z \in \bar {\mathcal Z}_s$. In this zone, the activation reward is maximum among all other zones for the agent in state $s$. Since the infection state remains unchanged, and migration is costly ($\cmig>0$), migrating to a different zone does not lead to a beneficial single-stage deviation for the agent.

We now consider an agent in zone $z \in {\mathcal Z}^{n}_s := {\mathcal Z} \setminus \{\bar {\mathcal Z}_s \cup \mathcal Z^0_s\}$. According to policy $\epi$, the agent does not migrate to a different zone. Consequently, the value function for the agent is
\[
    V[s,z](\epi,\sd) = \frac{{r}_\text{act}^*[s,z]}{1-\alpha}.
\]
Now, consider a single-stage deviation where the agent moves to location $z' \in \bar {\mathcal Z}_s$ and chooses activation degree $a' \in \mathcal{A}^*_{s,z}$. Consequently, using the definition of ${\mathcal Z}^{n}_s$,
\[
 Q[s,z,a',z'] =  r^*_\text{act}[s,z]-\cmig + \alpha \frac{\bar{r}_\text{act}[s]}{1-\alpha} < V[s,z](\epi,\sd).
\]
In other words, an agent in a zone in ${\mathcal Z}^{n}_s$ does not find it beneficial to migrate anywhere else in a single-stage deviation. 

It remains to show that an agent in zone $z \in \mathcal{Z}^0_s$ finds it beneficial to move to a zone in $\bar{\mathcal{Z}_s}$ and consequently, we must have $d[s,z]=0$ for $z \in \mathcal{Z}^0_s$. Under policy $\epi$, we have
$$ \epi[a \notin \mathcal{A}^*_{s,z}, z' \notin \bar{\mathcal{Z}}_s|s,z] = 0.$$
Consequently, the value function for the agent is
\begin{align*}
V[s,z](\epi,\sd) & = r^*_\text{act}[s,z] - \cmig + \alpha \frac{\bar{r}_\text{act}[s]}{1-\alpha} 
%\\ & > r^*_\text{act}[s,z] + \frac{\alpha}{1-\alpha} r^*_\text{act}[s,z] = 
> \frac{r^*_\text{act}[s,z]}{1-\alpha};
\end{align*}
in other words, the policy $\epi$ yields a higher value compared to any policy that does not include migration. Similarly, one can show that migrating to a zone in ${\mathcal Z}^{n}_s$ instead does not yield a beneficial single-stage deviation since $r^*_\text{act}[s,z'] < \bar{r}_\text{act}[s]$ for any zone $z' \in {\mathcal Z}^{n}_s$. \qed