\section{Equilibrium Analysis}
\label{sec:equilibrium}

We start by introducing the notion of best response based on the single-stage deviation reward defined in \eqref{eq:definitionQ}.

\begin{definition}[Best Response] The best response of an agent in state $(s,z)$ at the social state $(\pi,d)$ is the set valued correspondence $B_{s,z} : \Pi \times \D \rightrightarrows \Delta(\A \times \Z)$ given by
\begin{multline}
    \label{eq:BestResponse}
    %B_{s,z}(\pi,d) \in \Big\{\pi'(\cdot \mid s,z) \in \Delta(\U) : \\ \sum_{a,\mz} \left(\pi'[a,\mz \mid s,z] - \pi''[a,\mz \mid s,z] \right)\: Q[s,z,a,\mz](\pi,d) \geq 0, \\ \forall \pi''(\cdot \mid s,z) \in \Delta(\U)\Big\}.
    B_{s,z}(\pi,d) \in \Big\{\sigma \in \Delta(\A \times \Z) : \forall \sigma' \in \Delta(\A \times \Z) \\ \sum_{a,\mz} \left(\sigma[a,\mz] - \sigma'[a,\mz] \right)\: Q[s,z,a,\mz](\pi,d) \geq 0 \Big\}.
\end{multline}
\end{definition}

The above notion of best response is from the perspective of an individual agent in state $(s,z)$ when all other agents are following the homogeneous policy $\pi$ and their states are distributed as per $d$. The agent will choose any distribution $\sigma$ over the actions which maximizes its expected single-stage deviation reward $Q$ at the current state $(s,z)$. 

%\ezz{Such a $\sigma$ is guaranteed to exist since there is a finite set of actions.}

Consequently, a social state $(\epi,\sd)$ is stationary when agents in all states are playing their best response when they follow the policy $\epi$, and  the state distribution $\sd$ is stationary under this policy. Thus, we have the following definition of a stationary equilibrium. 

\begin{definition}[Stationary equilibrium]
A stationary equilibrium is a social state $(\epi,\sd) \in \Pi \times \D$ which satisfies
\begin{align}
    \epi[\cdot \mid s,z] &\in B_{s,z}(\epi,\sd), \; \forall (s,z) \in \St \times \Z, \label{eq:SE-1} \tag{SE.1}\\
    \sd &= \sd \: P(\epi,\sd). \label{eq:SE-2} \tag{SE.2}
\end{align}
\end{definition}

%The stationary equilibrium is a condition in which 

Thus, at the equilibrium, the state Markov chain $P(\epi,\sd)$ \eqref{eq:stochasticMatrix} is time-homogeneous, and the agents behave optimally in the corresponding Markov decision process~\cite{filar2012competitive}. \ezz{Note that we denote stationary equilibria with boldface notation.} \ezz{When $(\epi,\sd)$ is a stationary equilibrium, $\epi$ corresponds to the Nash equilibrium under state distribution $\sd$.} 

% Simultaneously, the equilibrium policy $\epi$ is optimal for the individual agent's resulting Markov decision process, so no agent will have an incentive to deviate from $\epi$\footnote{This follows from the definition of payoffs in terms of single stage deviations from the symmetrical policy $\pi$. At the equilibrium $(\epi,\sd)$, it is optimal to follow $\epi$ at all states, so $\epi$ solves the Bellman equation.}. Therefore, the social state $(\epi,\sd)$ is indeed an equilibrium.

\begin{theorem}[Theorem~1 in \cite{elokda2021dynamic}]
A stationary equilibrium $(\epi,\sd)$ for the proposed dynamic population game is guaranteed to exist.
\end{theorem}

We refer to~\cite{elokda2021dynamic} for the details of the proof, which relies on a fixed-point argument. \ezz{There, a general dynamic population game is considered in which the state and action spaces are finite, and} the state transition and reward functions are continuous in the social state $(\pi,d)$. \ezz{By definition, our model is an instance of such a dynamic population game.}

% \begin{proposition}
% \label{prop:StatAI}
% The stationary distribution $\sd$ does not have any asymptomatic or infected agents. If $\delta_\Unk^\Rec>0$, it also does not have any unknowingly recovered agents.
% \end{proposition}

The following proposition shows that the stationary distribution $\sd$ does not have any asymptomatic or infected agents (i.e., eventually, the epidemic dies out). The final stationary distribution is however not unique.

\begin{proposition}
\label{pro:characterizationeq}
Let ${r}_\textup{act}^*[s,z] := \max_a \ract[s,z,a]$ be the maximum activation reward in state $s$ and zone $z$, and let ${\mathcal A}_{s,z}^*$ be the set of activation levels that achieve such reward.
Let $\bar{r}_\textup{act}[s] := \max_z {r}_\textup{act}^*[s,z]$ be the maximum activation reward in state $s$ across all zones, and let $\delta_\Unk^\Rec, \alpha, \cmig > 0$. 
Let us define the subset of zones
\begin{align*}
\bar {\mathcal Z}_s & = \argmax_z {r}_\textup{act}^*[s,z], 
\\ 
    \mathcal Z^0_s & = \left\{ z \mid
     \bar{r}_\textup{act}[s] - {r}_\textup{act}^*[s,z] > \frac{1-\alpha}{\alpha} \cmig   
    \right\}.
%     \mathcal Z^0_s & = \left\{ z \mid
%  \frac{1-\alpha}{\alpha} \cmig + {r}_\textup{act}^*[s,z] <\bar{r}_\text{act}[s]   
% \right\}.
\end{align*}
Then, any social state $(\epi,\sd)$ satisfying
\begin{equation*}
    \begin{cases}
        \sd[\Asym,z] = \sd[\Inf,z] = \sd[\Unk,z] = 0 & \forall z, \\
        \sd[s,z \in \mathcal Z^0_s] = 0 &\forall s, \\
        \epi[a\notin \mathcal A^*_{s,z}, \mz \mid s,z] = 0 & \forall \mz, s, z, \\
        \epi[a, \mz\notin \bar{\mathcal Z}_s \mid s,z \in \mathcal Z^0_s] = 0 & \forall a, s,\\
        \epi[a, \mz \neq z \mid s,z \notin \mathcal Z^0_s] = 0 & \forall a, s.
    \end{cases}
\end{equation*}
is a stationary equilibrium.
\end{proposition}

%\footnotetext{An analogous result can be obtained when $\delta_\Unk^\Rec = 0$, in which case $\sd[\Unk,z]$ does not need to be zero. The case $\alpha=0$ is easily derived by interpreting $(1-\alpha)/\alpha$ as $+\infty$.}

%When players are playing the dominant activation strategy, the value function of zone $z$ (AH: for which policy?) is
%\[
%    V[s,z] = \frac{{r}_\text{act}^*[s,z]}{1-\alpha},
%\]
%and the value function in zone $\bar z[s] := \argmax_z {r}_\text{act}^*[s,z]$ is
%\[
%    V[s,\bar z[s]] = \frac{\bar{r}_\text{act}[s]}{1-\alpha}.
%\]
%Based on the definition of $Q$, the \emph{dominant migration strategy} for $s \in \set{\Rec,\Sus}$ (with no infections) is then not to move to (or stay in) $z$ if
%\[
%    -\cmig + \frac{\alpha}{1-\alpha} \: \bar{r}_\text{act}^*[s] > \frac{\alpha}{1-\alpha} \: {r}_\text{act}^*[s,z].
%\]
%and not move from $z$ if the strict ``$<$'' holds. If the two sides are equal, then moving or not yields the same payoff, but only not moving can be sustained at equilibrium, since no one will move back and $z$ will deplete. 

%(AH: are we allowed to move to a zone with r* within 1-alp/alp $\cmig$ of $\bar{r}$?)

The proof is presented in the extended version \cite{elokdadynamic_cdc_arxiv}. Proposition~\ref{pro:characterizationeq} shows how stationary equilibria can be computed without solving a fixed-point problem, and directly identifies some dominant strategies for the agents.
The identification of dominant strategies is insightful for the design of interventions (e.g., lockdown measure for the different compartments).
For example, it is possible to verify that
\[
\mathcal A_{\Rec,z}^* = 
\argmax_a\ 
\ract[\Rec, z, a] = 
\argmax_a\ 
\left(o[a] - c[\Rec, z, a]\right)
\]
corresponds to the dominant activation strategies for agents in $\Rec$ (\emph{knowingly} immune agents).
Notice that the activation caused by immune agents appears in the denominator of the probability that a generic agent interacts with an infectious agent -- see \eqref{eq:prob_gammas} -- \ezz{and} therefore looser lockdown measures for recovered agents can be used to reduce the spreading as shown in simulations in Section~\ref{sec:simulations}.

%     \item Suppose that $\cmig = 0$. Then any migration policy is possible at the equilibrium $\epi$.
% \end{enumerate}
% \end{proposition}

%\input{sections/proof}