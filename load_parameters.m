function param = load_parameters()
% Defines the parameters of the model, and assigns their values

%% State definition
% Infection state
param.S = 'SAIRU';
param.n_s = length(param.S);
param.s_S = find(param.S == 'S');
param.s_A = find(param.S == 'A');
param.s_I = find(param.S == 'I');
param.s_R = find(param.S == 'R');
param.s_U = find(param.S == 'U');

% Zone
param.n_z = 2;
param.Z = 1 : param.n_z;

%% Initial state distribution
% Initial zones distribution
switch param.n_z
    case 1
        param.d_init_up_z = 1.0;
    case 2
        param.d_init_up_z = [0.9, 0.1]; % Zone 1 is a city, zone 2 is a village
end

% Initial infection state-zone distribution
param.d_init_up_s_z = zeros(param.n_s, param.n_z);
% We specify the proportion of agents that are infected and asymptomatic in
% each zone, and the rest are susceptible
param.d_init_up_s_z(param.s_A,1) = 0.02 * param.d_init_up_z(1);  % Epidemic starts in the city
param.d_init_up_s_z(param.s_I,1) = 0.01 * param.d_init_up_z(1);
param.d_init_up_s_z(param.s_S,1) = param.d_init_up_z(1) - param.d_init_up_s_z(param.s_A,1) - param.d_init_up_s_z(param.s_I,1);
if param.n_z > 1
    param.d_init_up_s_z(param.s_A,2) = 0.0 * param.d_init_up_z(2);  % No infections in the village
    param.d_init_up_s_z(param.s_I,2) = 0.0 * param.d_init_up_z(2);  % No infections in the village
    param.d_init_up_s_z(param.s_S,2) = param.d_init_up_z(2) - param.d_init_up_s_z(param.s_A,2) - param.d_init_up_s_z(param.s_I,2);
end

assert(abs(sum(param.d_init_up_s_z(:)) - 1) < 1e-10, 'Invalid initial state distribution');

%% Action definition
% Activation degree
param.a_max = 6;
param.A = 0 : 1 : param.a_max;
param.n_a = length(param.A);

% Migration zone
% This has the same action space as the zone state space
% We will reger to the migration action as 'az'

%% Initial policy
% % Uniformly random actions
% param.pi_init_down_s_z_up_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
% for i_s = 1 : param.n_s
%     for i_z = 1 : param.n_z
%         param.pi_init_down_s_z_up_a_az(i_s,i_z,:,:) = 1 / (param.n_a * param.n_z) * ones(param.n_a, param.n_z);
%     end
% end

% % No migrations, and uniformly random activation degrees
% param.pi_init_down_s_z_up_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
% for i_s = 1 : param.n_s
%     for i_z = 1 : param.n_z
%         param.pi_init_down_s_z_up_a_az(i_s,i_z,:,i_z) = 1 / param.n_a * ones(param.n_a, 1);
%     end
% end

% No migrations, and uniformly random activation degrees, except infected
% agents who do not activate
param.pi_init_down_s_z_up_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
for i_s = 1 : param.n_s
    for i_z = 1 : param.n_z
        if i_s ~= param.s_I
            param.pi_init_down_s_z_up_a_az(i_s,i_z,:,i_z) = 1 / param.n_a * ones(param.n_a, 1);
        else
            param.pi_init_down_s_z_up_a_az(i_s,i_z,1,i_z) = 1;
        end
    end
end

% % No migrations, and maximum activation degree
% param.pi_init_down_s_z_up_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
% for i_s = 1 : param.n_s
%     for i_z = 1 : param.n_z
%         param.pi_init_down_s_z_up_a_az(i_s,i_z,1,i_z) = 1;
%     end
% end

% % No migrations, and maximum activation degree, except infected agents who
% % do not activate
% param.pi_init_down_s_z_up_a_az = zeros(param.n_s, param.n_z, param.n_a, param.n_z);
% for i_s = 1 : param.n_s
%     for i_z = 1 : param.n_z
%         if i_s ~= param.s_I
%             param.pi_init_down_s_z_up_a_az(i_s,i_z,end,i_z) = 1;
%         else
%             param.pi_init_down_s_z_up_a_az(i_s,i_z,1,i_z) = 1;
%         end
%     end
% end

param.pi_init_down_s_z_up_a_az = param.pi_init_down_s_z_up_a_az ./ sum(param.pi_init_down_s_z_up_a_az, [3, 4]);   % Corrects for small numerical drifts
assert(norm(sum(param.pi_init_down_s_z_up_a_az, [3, 4]) - 1, inf) < 1e-10, 'Invalid initial policy');

%% State transition parameters
% Probability of infection (S -> A) for each asymptomatic neighbor
param.beta_A = 0.2;

% Probability of infection (S -> A) for each symptomatic neighbor
param.beta_I = 0.2;

% Ficticious amount of activation for when activity is minute
param.epsilon = 1e-2;

% Probability to transition from asymptomatic to symptomatic (A -> I)
param.delta_down_A_up_I = 0.08;

% Probability to transition from asymptomatic to unknowingly recovered (A -> U)
param.delta_down_A_up_U = 0.08;

% Probability to transition from infected to recovered (I -> R)
param.delta_down_I_up_R = 0.04;

% Probability to transition from unknowingly recovered to recovered (U -> R)
param.delta_down_U_up_R = 0.01;

% Is the proportion of infected but asymptomatic agents observed?
param.observe_A = true;

%% Stage reward parameters
% Reward for activation (degree dependent)
param.o_down_a = param.A / param.a_max; % Simply linear

% Cost for activation (infection state, zone, and degree dependent)
param.c_down_s_z_a = zeros(param.n_s, param.n_z, param.n_a);

% Cost for activation when susceptible, asymptomatic, or unkowingly recovered
param.a_lock = zeros(1, param.n_z); % Lockdown activation degree, which is zone dependent. Higher activations are severely penalized
% param.a_lock(:) = 2;   % Same in all zones
param.a_lock(1) = 4;
param.a_lock(2) = 2;

i_a_lock = param.a_lock + 1;
for i_z = 1 : param.n_z
    % Specify as a multiplier of the reward for activation. Low multiplier
    % for activations below lockdown, high multiplier above
    param.c_down_s_z_a(param.s_S,i_z,1:i_a_lock(i_z)) = 0.0 * param.o_down_a(1:i_a_lock(i_z));
    param.c_down_s_z_a(param.s_S,i_z,i_a_lock(i_z)+1:end) = 3.0 * param.o_down_a(i_a_lock(i_z)+1:end);
end

param.c_down_s_z_a(param.s_A,:,:) = param.c_down_s_z_a(param.s_S,:,:);
param.c_down_s_z_a(param.s_U,:,:) = param.c_down_s_z_a(param.s_S,:,:);

% Cost for activation when symptomatic
param.c_down_s_z_a(param.s_I,:,:) = repmat(3.0 * param.o_down_a, param.n_z, 1);  % Specify as a multiplier of the reward for activation

% Cost for activation when recovered
param.R_lock = false;    % Shutdown R as well?
if param.R_lock
    param.c_down_s_z_a(param.s_R,:,:) = param.c_down_s_z_a(param.s_S,:,:);
else
    param.c_down_s_z_a(param.s_R,:,:) = repmat(0.0 * param.o_down_a, param.n_z, 1);  % Specify as a multiplier of the reward for activation
end

% Cost of the illness
param.c_d = 10 * param.o_down_a(end);   % The suffering of illness is worse than the reward for activating with max degree

% Cost for migration
param.c_m = 2 * param.o_down_a(end);   % Also higher than the reward for activating with max degree; moving is costly

%% Future discount factor
param.alpha = 0.9;

%% Bounded rationality
param.lambda = 10;

%% Computation parameters
% Tolerance of policy
param.se_pi_tol = 1e-6;

% Tolerance of state distribution
param.se_d_tol = 1e-6;

% Maximum timesteps
param.K = 100000;

% Tolerance of value function
param.V_tol = 1e-6;

% Max recursions of value function
param.V_K = 10000;

% Discrete step size
% param.dt = 0.01;
param.dt = 1.00;

% Policy update rate
param.eta = 0.1;

%% Data logging parameters
% Parameter of interest for logging
% 0 (default) = no logging
% 1 = alpha
% 2 = c_m
% 3 = c_I
% 4 = c_S
% 5 = delta_U_R
% 6 = a_lockdown
param.log = 0;

end